/**
 * Задание 1.
 *
 * Создать объект пользователя, который обладает тремя свойствами:
 * - Имя;
 * - Фамилия;
 * - Профессия.
 *
 * А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.
 */

const user = {
   firstName: 'Viktor',
   lastName: 'Kostenko',
   profession: 'Coder',
   sayHi() {
      console.log(`Hi, ${this.firstName}!`);
   }
}

user.sayHi();