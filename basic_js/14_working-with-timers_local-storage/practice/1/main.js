/**
 * Задание 1
 * 
 * Сделать счетчик, который каждую секунду будет увеличиваться на один 
 * 
 * Advanced: 
 * 
 * - Дать возможность запускать, останавливать, скидывать счетчик
 * - Если счетчик запущен, делать кнопку Start и Reset не активными
 * - Если счетчик остановлен, то только кнопка Pause не активна
 * - Использовать setInterval и clearInterval
 * 
 */

const counterElem = document.querySelector('#counter');
const buttonsContainer = document.querySelector('.buttons');
const buttons = document.querySelectorAll('button');

let counter = 0;
let startInterval;
let pressingCount = 0;

function startCounter() {
	startInterval = setInterval(() => {
		counter++;
		counterElem.textContent = counter;
	}, 1000);
};

function pauseCounter() {
	clearInterval(startInterval);
}

buttonsContainer.addEventListener('click', e => {

	const target = e.target;

	if (target.tagName !== 'BUTTON') return;


	switch (target.textContent) {
		case 'start':
			buttons.forEach(e => {
				if (e.textContent !== 'pause') {
					e.classList.add('--disabled')
				} else {
					e.classList.remove('--disabled')
				}
			});
			if (pressingCount === 0) {
				startCounter();
			}
			pressingCount++;
			break;
		case 'pause':
			buttons.forEach(e => {
				if (e.textContent === 'pause' && pressingCount > 0) {
					e.classList.add('--disabled')
				} else {
					e.classList.remove('--disabled')
				}
			});
			pauseCounter();
			pressingCount = 0;
			break;
		case 'reset':
			pauseCounter();
			buttons.forEach(e => e.classList.remove('--disabled'));
			counter = 0;
			counterElem.textContent = counter;
	}

})
