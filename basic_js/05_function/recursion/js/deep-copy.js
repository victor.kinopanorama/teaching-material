let originalArray = [
	37,
	3900,
	{
		hello: 'world',
		skills: {
			js: 100,
			css: 100,
			html: 100,
		}
	}
];

const deepCopyFunction = (inObject) => {

	let outObject = null;

	if (typeof inObject !== 'object' || inObject === null) {
		return inObject; // Return the value if inObject is not an object
	}

	if (Array.isArray(inObject)) {
		// Create an array to hold the values
		outObject = [];

		// Recursively (deep) copy for nested objects, including arrays
		inObject.forEach(value => outObject.push(deepCopyFunction(value)))
	} else {
		// Create an object to hold the values
		outObject = {};

		let value = null;
		let key = null;

		for (key in inObject) {
			value = inObject[key];

			// Recursively (deep) copy for nested objects, including arrays
			outObject[key] = deepCopyFunction(value);
		}
	}

	return outObject;
}

deepCopyFunction(originalArray);

