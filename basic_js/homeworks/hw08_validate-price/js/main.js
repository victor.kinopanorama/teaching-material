const container = document.querySelector('.container');
const inputElem = document.querySelector('input');
const currentPriceContainer = document.querySelector('.current-price');


inputElem.addEventListener('focus', () => {

	if (container.querySelector('p.error-text')) {
		container.querySelector('p.error-text').remove();
	}

	inputElem.style.border = '2px solid green';

});

inputElem.addEventListener('blur', () => {

	inputElem.style.border = '';

	const inputValue = inputElem.value;

	if (isNaN(inputValue) || inputValue < 0 || inputValue === '') {

		inputElem.style.border = '2px solid rgb(114, 0, 0)';

		const errorText = document.createElement('p');

		// Если не присутствует текст с ошибкой на странице, тогла создаем и вставляем
		if (!container.querySelector('p.error-text')) {

			errorText.classList.add('error-text')
			errorText.textContent = 'Please enter the correct price';

			container.append(errorText);

		}

	} else {

		currentPriceContainer.insertAdjacentHTML('beforeend', `<span>Current price: ${inputValue}$</span><button>x</button>`)

		inputElem.style.color = 'green';

	}

})

currentPriceContainer.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName !== 'BUTTON') return;

	target.previousElementSibling.remove();
	target.remove();

})