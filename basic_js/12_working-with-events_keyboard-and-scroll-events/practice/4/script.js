/**
 * При натисканні на enter в полі вводу
 * додавати його значення, якщо воно не пусте,
 * до списку задач та очищувати поле вводу
 *
 * При натисканні Ctrl + Z на сторінці видаляти
 * останню додану задачу
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt + Shift + Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok / Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */

const input = document.querySelector('#new-task');
const tasksList = document.querySelector('.tasks-list');
const clearBtn = document.querySelector('#clear');

input.addEventListener('keyup', e => {

	if (e.key === 'Enter' && input.value !== '') {

		let task = document.createElement('li');
		task.textContent = inputValue;
		tasksList.append(task);

		input.value = ''

	}

})

window.addEventListener('keyup', e => {

	if (e.ctrlKey && e.key === 'z') {

		let lastElem = tasksList.querySelector('li:last-child');

		if (lastElem) lastElem.remove(); // удалять, когда в списке есть хотя бы один элемент

	}

	if (e.altKey && e.shiftKey && e.code === 'Backspace') {

		isAgree();

	}

})

clearBtn.addEventListener('click', () => isAgree());

// функция запрашивает подтверждение на удаление
function isAgree() {

	let result = confirm('Are you sure you want to clear the list?');

	if (result) {

		//* 1
		tasksList.querySelectorAll('li').forEach(e => e.remove());

		//* 2
		// tasksList.innerHTML = '';

	}

}