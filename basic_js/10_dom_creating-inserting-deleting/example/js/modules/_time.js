setInterval(() => {

	let date = new Date();

	const hour = document.querySelector('.time__hour');
	const minutes = document.querySelector('.time__minutes');
	const seconds = document.querySelector('.time__seconds');

	hour.textContent = date.getHours();

	(date.getMinutes() <= 9) ? minutes.textContent = `0${date.getMinutes()}` : minutes.textContent = date.getMinutes();
	(date.getSeconds() <= 9) ? seconds.textContent = `0${date.getSeconds()}` : seconds.textContent = date.getSeconds();

}, 1000)
