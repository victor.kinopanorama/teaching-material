/**
 * Задание 2.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'HELLO WORLD!';

function getRandomColor() {
	const r = Math.floor(Math.random() * 255);
	const g = Math.floor(Math.random() * 255);
	const b = Math.floor(Math.random() * 255);

	return `rgb(${r}, ${g}, ${b})`;
}

/* Решение */
// 1. Создать h1
// 2. Добавить ему текст
// 3. Добавить его на страницу
// 4. Создать button
// 5. Добавить ему текст
// 6. Добавить его на страницу
// 7. Долбавить кнопке обработчик события.
// 8. В обработчике:
// 8.1. Получить текст из элемента h1
// 8.2. Очистить внутренний HTML H1
// 8.3. Используя for of:
// 8.3.1 Для каждой буквы создать элемент <span>
// 8.3.2 Для элемента <span> назначить цвет возвращаемый getRandomColor()
// 8.3.3 Добавить элемент <span> внутрь h1

const title = document.createElement('h1');
const paintBtn = document.createElement('button');

paintBtn.textContent = 'to paint';
title.innerText = PHRASE;

document.body.prepend(title);
document.body.append(paintBtn);

paintBtn.addEventListener('click', () => {

	let titleText = title.innerText;
	title.innerHTML = '';

	for (let char of titleText) {

		title.insertAdjacentHTML('beforeend', `<span style="color: ${getRandomColor()}">${char}</span>`);

	}

});