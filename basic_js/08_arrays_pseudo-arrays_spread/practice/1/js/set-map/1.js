/**
 * Задача 1.
 *
 * Написать функцию-помощник покупателя.
 *
 * Функция не обладает параметрами и возвращает массив.
 * Функция запрашивает у пользователя строку с товарами, которые он желает приобрести.
 * Строка должна содержать все товары через запятую.
 *
 * Программа повторно опрашивает пользователя если:
 * - Не введено товаров вообще.
 *
 * После чего программа выводит в консоль введённые пользователем товары,
 * при этом исключив из списка дубликаты.
 *
 * Условия:
 * - Обязательно использовать структуру данных Set.
 */

function getItemList() {

	let userProductList = prompt('Enter a list of items');

	while (userProductList === '' || !isNaN(userProductList)) {

		if (userProductList === null) break;

		userProductList = prompt('Enter a list of items');

	}

	if (userProductList !== null) {

		return new Set(userProductList.split(','))

	}

}

console.log(getItemList());
// tea,sugar,cakes,coffee,sugar,macarons,tea
