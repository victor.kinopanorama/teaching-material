const inputElems = document.querySelectorAll('input.form__field');
const createAccounBtn = document.querySelector('.form__btn');

const firstNameInputElem = document.querySelector('input[name="first_name"]');
const lastNameInputElem = document.querySelector('input[name="last_name"]');
const passwordElem = document.querySelector('input[name="password"]');
const confirmPasswordElem = document.querySelector('input[name="confirm_password"]');
const emailInputElem = document.querySelector('input[name="email"]');
const checkboxInputElem = document.querySelector('.form__checkbox-input');

const errorPasswordSpanElem = document.querySelector('.error-password');
const errorEmailSpanElem = document.querySelector('.error-email');

let emailValide = false;

// Функция валидации поля имейл
emailInputElem.addEventListener('blur', event => {

	const target = event.target;

	if (!target.value.match(/[a-z0-9]@\w+\.\w+/)) {

		emailInputElem.style.border = '1px solid red';
		errorEmailSpanElem.textContent = 'invalid email';
		errorEmailSpanElem.classList.add('visible');

	} else {

		emailValide = true;

	}

});

emailInputElem.addEventListener('focus', () => {

	errorEmailSpanElem.classList.remove('visible');
	emailInputElem.style.border = '';
	errorEmailSpanElem.textContent = '';

})

// Функция валидации паролей 
function checkPassword() {

	if (passwordElem.value === '' && confirmPasswordElem.value === '') {

		errorPasswordSpanElem.textContent = 'fill in both fields';
		errorPasswordSpanElem.classList.add('visible');

		passwordElem.style.border = '1px solid red';
		confirmPasswordElem.style.border = '1px solid rgb(211, 13, 13)';

	} else if (passwordElem.value === '') {

		passwordElem.style.border = '1px solid rgb(211, 13, 13)';
		errorPasswordSpanElem.classList.add('visible');
		errorPasswordSpanElem.textContent = 'fill in the field';

	} else if (confirmPasswordElem.value === '') {

		confirmPasswordElem.style.border = '1px solid rgb(211, 13, 13)';
		errorPasswordSpanElem.classList.add('visible');
		errorPasswordSpanElem.textContent = 'fill in the field';

	} else if (passwordElem.value !== confirmPasswordElem.value) {

		errorPasswordSpanElem.textContent = 'Passwords mismatch';
		errorPasswordSpanElem.classList.add('visible');

		passwordElem.style.border = '1px solid red';
		confirmPasswordElem.style.border = '1px solid rgb(211, 13, 13)';

	} else {

		return true;

	}

}

passwordElem.addEventListener('focus', () => {

	passwordElem.style = '';
	confirmPasswordElem.style = '';
	errorPasswordSpanElem.classList.remove('visible');

});

confirmPasswordElem.addEventListener('focus', () => {

	passwordElem.style = '';
	confirmPasswordElem.style = '';
	errorPasswordSpanElem.classList.remove('visible');

});

const newUser = {};
let numberOfUser = 1;

createAccounBtn.addEventListener('click', e => {

	e.preventDefault();

	if (firstNameInputElem.value !== '' &&
		lastNameInputElem.value !== '' &&
		isNaN(firstNameInputElem.value) &&
		isNaN(lastNameInputElem.value)) {

		if (emailValide && checkPassword() && checkboxInputElem.checked) {

			inputElems.forEach(elem => {

				newUser[elem.name] = elem.value;

			});

			localStorage[`user-${numberOfUser}`] = JSON.stringify(newUser);
			numberOfUser++;

		}

	}

});

console.log(numberOfUser);

