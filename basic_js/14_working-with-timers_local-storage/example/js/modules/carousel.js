const btnPrev = document.querySelector('.btn-prev');
const btnNext = document.querySelector('.btn-next');
const galleryList = document.querySelector('.gallery__list');
const galleryListItems = document.querySelectorAll('.gallery__list-item');


let imageWidth = 130;
let imageCount = 3;
let position = 0;

btnNext.addEventListener('click', () => {
	position -= imageWidth * imageCount;

	position = Math.max(position, -imageWidth * (galleryListItems.length - imageCount));
	galleryList.style.marginLeft = position + 'px';

})

btnPrev.addEventListener('click', () => {
	position += imageWidth * imageCount;

	position = Math.min(position, 0)
	galleryList.style.marginLeft = position + 'px';

})


