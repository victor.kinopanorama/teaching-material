/**
 * Задание 1.
 *
 * Написать имплементацию встроенной функции строки repeat(times).
 *
 * Функция должна обладать двумя параметрами:
 * - Целевая строка для повторения;
 * - Количество повторений целевой строки.
 *
 * Функция должна возвращать преобразованную строку.
 *
 * Условия:
 * - Генерировать ошибку, если первый параметр не является строкой,
 * а второй не является числом.
 * 
 */


/* Решение */

const repeatString = (str, numOfRepeat) => {

	if (!isNaN(str)) throw new Error('First parameter should be a string type');
	if (isNaN(numOfRepeat)) throw new Error('Second parameter should be a repeatnumber type');

	let result = '';

	for (let i = 0; i < numOfRepeat; i++) result += `- ${str} `;

	return result;
}

console.log(repeatString('Hello World', 5));