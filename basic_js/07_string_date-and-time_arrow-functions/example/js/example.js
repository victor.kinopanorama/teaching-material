const title = document.querySelector('.title');
const img = document.querySelector('.img');

title.addEventListener('click', e => {
	title.classList.toggle('text-blur');
})

img.addEventListener('click', () => {
	img.classList.toggle('img-scale');
})

/**
 * 
 * Выводить в консоль, сколько дней прошло с начала года до даты, которую ввел пользователь.
 * 
 */

const minute = 1;
const hour = minute * 60;
const day = hour * 24;
const year = day * 365;

console.log(day);