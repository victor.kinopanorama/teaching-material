/**
 * 
 * Провести следующие операции и вывести результат каждой в консоль:
 * - узнать сколько в общем товаров на складе.
 * - узнать, какого товара на складе больше всего.
 * - спросить у юзера, колличество какого товара он хочет узнать
 * и вывести результат в alert.
 * - указать колличество телефонов 12 шт, а ноутбуков 3 шт.
 * - узнать, какого товара на складе больше сейчас.
 * - указать колличество наушников 32шт, ноутбуков 100 шт.
 * - вывести общее колличество товаров на складе и вместительность склада
 * 
 * 
 */

const warehouse = {
	maxCapacity: 30,
	isAvailable: false,
	products: {
		phones: 2,
		headphones: 4,
		notebooks: 10,
		tablets: 7,
	},

	getTotalQuantity() {
		let sum = 0;

		for (const item in this.products) {
			sum += this.products[item]
		}

		return sum;
	},

	getMaxQuantity() {
		let max = 0;
		let maxProductName;

		for (const item in this.products) {
			if (this.products[item] > max) {
				max = this.products[item];
				maxProductName = item;
			}
		}

		return maxProductName;
	},

	getProductQuantity() {
		let productName = prompt('Enter the product name');
		let productNameLower = productName.toLowerCase();

		//* Вариант 1
		// for(const item in this.products) {
		//    if(item === productNameLower) {
		//       alert(`Number of ${productNameLower}: ${this.products[item]}`)
		//       this.isAvailable = true;
		//    }
		// }

		// if(!this.isAvailable) {
		//    return `${userInput} not found!`;
		// }

		// this.isAvailable = false;

		//* Вариант 2
		// Если phones:0 - будет undefined
		if (typeof this.products[productNameLower] === 'undefined') {
			return `${productName} not found!`;
		} else {
			alert(`Number of ${productNameLower}: ${this.products[productNameLower]}`)
		}
	},

	changeProductQuantity() {
		let productName = prompt('Enter the product name');
		let productNameLower = productName.toLowerCase();
		let productQuantity = +prompt('Enter the product quantity');

		if (productQuantity < 0) {
			console.log('Error! Not enough capacity');
			return;
		}

		if (this.getTotalQuantity() - this.products[productNameLower] + productQuantity <= this.maxCapacity) {
			this.products[productNameLower] = productQuantity;
		} else {
			console.log('Error! Max capacity')
		}
	}
}

// console.log(warehouse.getTotalQuantity());
// console.log(warehouse.getMaxQuantity());
// console.log(warehouse.getProductQuantity());

// warehouse.products.phones = 12;
// warehouse.products.notebooks = 3;
// console.log(warehouse.getMaxQuantity());

// warehouse.products.headphones = 32;
// warehouse.products.notebooks = 100;

warehouse.changeProductQuantity();
console.log(warehouse.products);

