//* MAP
// Map (Структура данных) – это коллекция ключ/значение, как и Object. Но основное отличие в том, что Map позволяет использовать ключи любого типа.

const obj = {
	name: 'Viktor',
	age: 35,
	job: 'Fullstack'
}

//? Object.entries(obj) преобразует обьект в массив [ ['name', 'Viktor'], ['age', '35'] ];
// console.log(Object.entries(obj));

const entries = [
	['name', 'Viktor'],
	['age', '35'],
	['job', 'Fullstack']
]

//? Object.fromEntries(entries) преобразует массив в объект
// console.log(Object.fromEntries(entries));

const map = new Map([
	['name', 'Viktor'],
	['age', '35'],
	['job', 'Fullstack']
])

//? доступ к значению ключа
// console.log(map.get('name'));

// задаем ключ-значение (ключи любого типа)
map
	.set('newField', 1488)
	.set(obj, 'Value of object')
	.set(NaN, 'NaN ?')

//? удаление ключа
// map.delete('newField')

//? проверка на наличие ключа
// console.log(map.has('newField'));

//? сколько ключей в коллекции
// console.log(map.size);

//? метод очищает всю коллекцию
// map.clear()

// ===================================================================

//? перебор коллекции
// for (let [key, value] of map) {
// 	console.log(key, value);
// }

// for (let key of map.keys()) {
// 	console.log(key);
// }

// map.forEach((value, key) => console.log(`${key}: ${value}`))

// ===================================================================

//? Массив из карты
// const arr = [...map];
// const arr1 = Array.from(map);

// ===================================================================

// Практический пример (когда пользователь заходил на страницу)
const users = [
	{ name: 'Elena' },
	{ name: 'Alex' },
	{ name: 'Viktor' }
]

const visits = new Map()

visits
	.set(users[0], new Date())
	.set(users[1], new Date(new Date().getTime()))
	.set(users[2], new Date(new Date().getTime()))

function lastVisit(user) {
	return visits.get(user)
}

console.log(lastVisit(users[0]));