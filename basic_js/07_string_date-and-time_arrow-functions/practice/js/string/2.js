/**
 * Задание 2.
 *
 * Написать функцию, capitalizeAndDoublify, которая переводит
 * символы строки в верхний регистр и дублирует каждый её символ.
 *
 * Условия:
 * - Использовать встроенную функцию repeat;
 * - Использовать встроенную функцию toUpperCase;
 * - Использовать цикл for...of.
 */

/* Решение */
function capitalizeAndDoublify(str) {

	let result = '';

	for (let char of str) {
		result += char.repeat(2).toUpperCase();
	}

	return result;
}

console.log(capitalizeAndDoublify('hello')); // HHEELLLLOO
console.log(capitalizeAndDoublify('JavaScript!')); // JJAAVVAASSCCRRIIPPTT!!