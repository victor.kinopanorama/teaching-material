const hour = document.querySelector('.time__hour');
const minutes = document.querySelector('.time__minutes');
const seconds = document.querySelector('.time__seconds');

const dayNum = document.querySelector('.day__number');
const dayMonth = document.querySelector('.day__month');

const dayOfTheWeek = document.querySelector('.day__name');

let dateNowClg = new Date();

function setDate() {

	let dateNow = new Date();
	let day = dateNow.getDay();


	if (dateNow.getHours() < 10) {
		hour.textContent = '0' + dateNow.getHours();
	} else {
		hour.textContent = dateNow.getHours();
	}

	if (dateNow.getMinutes() < 10) {
		minutes.textContent = '0' + dateNow.getMinutes();
	} else {
		minutes.textContent = dateNow.getMinutes();
	}

	if (dateNow.getSeconds() < 10) {
		seconds.textContent = '0' + dateNow.getSeconds();
	} else {
		seconds.textContent = dateNow.getSeconds();
	}

	dayNum.textContent = dateNow.getDate();
	setMonth(dateNow.getMonth());

	switch (day) {
		case 0:
			dayOfTheWeek.textContent = 'Воскресенье';
			break;
		case 1:
			dayOfTheWeek.textContent = 'Понедельник';
			break;
		case 2:
			dayOfTheWeek.textContent = 'Вторник';
			break;
		case 3:
			dayOfTheWeek.textContent = 'Среда';
			break;
		case 4:
			dayOfTheWeek.textContent = 'Четверг';
			break;
		case 5:
			dayOfTheWeek.textContent = 'Пятница';
			break;
		case 6:
			dayOfTheWeek.textContent = 'Суббота';
			break;
	}

}

function setMonth(month) {
	const monthArr = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь'];

	if (monthArr[month].endsWith('ь') || monthArr[month].endsWith('й')) {
		dayMonth.textContent = `${monthArr[month].slice(0, -1)}я`
	} else if (monthArr[month].endsWith('т')) {
		dayMonth.textContent = `${monthArr[month]}а`
	}

}

setInterval(() => setDate(), 1000);