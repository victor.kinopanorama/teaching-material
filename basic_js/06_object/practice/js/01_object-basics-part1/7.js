/**
 * 
 * Создайте пустой объект notebook2 и скопируйте в него все поля из объекта notebook. 
 * - Поменяйте в notebook2 значения поля name на "Acer Swift 5" и size.width на 16
 * - выведите оба объекта в консоль. 
 * 
 * Подсказка: используйте for..in и typeof
 * 
 */

const notebook = {
	name: "LG Gram",
	price: "36000",
	size: {
		width: 15,
		height: 10.5,
		depth: 0.7
	}
};

const notebook2 = {};

for (let key in notebook) {
	if (typeof notebook[key] === 'object' && notebook[key] !== null) {

		notebook2[key] = {};

		for (let innerKey in notebook[key]) {
			notebook2[key][innerKey] = notebook[key][innerKey];
		}

	} else {

		notebook2[key] = notebook[key];

	}
}

notebook2.name = "Acer Swift 5";
notebook2.size.width = 16;

console.log(notebook);
console.log(notebook2);