/**
 * Задание 4.
 *
 * Написать программа для редактирования количества остатка продуктов на складе магазина.
 *
 * Программа должна запрашивать название товара для редактирования.
 * Если ввёденного товара на складе нет — программа проводит повторный запрос названия товара
 * до тех пор, пока соответствующее название не будет введено.
 *
 * После чего программа запрашивает новое количество товара.
 * После чего программа вносит изменения на веб-страницу: заменяет остаток указанного товара его новым количеством.
 */

const productList = document.querySelectorAll('.store li');

let userProductName;
let isAvailable = true;

do {

	userProductName = prompt('Enter product name');

	if (userProductName === null) break;

	productList.forEach(elem => {

		let splitedElem = elem.innerText.split(': ');

		if (splitedElem[0] === userProductName) isAvailable = false;
	})


} while (isAvailable);

if (userProductName) {

	let userProductValue = prompt('Enter product quantity');

	while (isNaN(userProductValue) || userProductValue === '' || userProductValue < 0) {

		userProductValue = prompt('Enter product quantity');

	}

	productList.forEach(elem => {

		let splitedElem = elem.innerText.split(': ');

		if (splitedElem[0] === userProductName) {

			elem.innerText = `${userProductName}: ${userProductValue}`

		}
	})

} 