/**
 * 
 * При наведенні на блок 1 робити
 * блок 2 зеленим кольором
 * А при наведенні на блок 2 робити
 * блок 1 червоним кольором
 * 
 * Коли курсор покидає блок 1, то робити блок 2 білим.
 * Коли курсор покидає блок 2, то робити блок 1 білим.
 * 
 */

const blockNumberOne = document.querySelector('#block-1');
const blockNumberTwo = document.querySelector('#block-2');

blockNumberOne.addEventListener('mouseenter', () => blockNumberTwo.style.backgroundColor = 'green');
blockNumberOne.addEventListener('mouseleave', () => blockNumberTwo.style.backgroundColor = '');

blockNumberTwo.addEventListener('mouseenter', () => blockNumberOne.style.backgroundColor = 'red');
blockNumberTwo.addEventListener('mouseleave', () => blockNumberOne.style.backgroundColor = '');

