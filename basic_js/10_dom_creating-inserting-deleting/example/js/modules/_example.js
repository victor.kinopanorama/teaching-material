const container = document.querySelector('.example');
const div = document.createElement('div');
const list = document.querySelector('ul');

const upBtn = document.createElement('button');
const downBtn = document.createElement('button');
const topBtn = document.createElement('button');

upBtn.textContent = 'up';
downBtn.textContent = 'down';
topBtn.textContent = 'top';

upBtn.classList.add('up-button');
downBtn.classList.add('down-button');
topBtn.classList.add('top-button');

document.body.prepend(upBtn);
document.body.append(downBtn);
document.body.append(topBtn);

upBtn.addEventListener('click', () => window.scrollBy(0, -30));
downBtn.addEventListener('click', () => window.scrollBy(0, 30));
topBtn.addEventListener('click', () => {
	window.scrollTo({
		top: 0,
		left: 0,
		behavior: "smooth" // плавный переход
	})
})

//* Отключить прокрутку
// document.body.style.overflow = "hidden" 

/**
 * Задание 2.
 *
 * Написать функцию-фабрику квадратов createSquares.
 *
 * Функция обладает одним параметром — количеством квадратов для создания.
 *
 * Если пользователь ввёл количество квадратов для создания в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Максимальное количество квадратов для создания — 10.
 * Если пользователь решил создать более 10-ти квадратов — сообщить ему о невозможности такой операции
 * и запрашивать данные о количестве квадратов для создания до тех пор, пока они не будут введены корректно.
 *
 * Размер каждого квадрата в пикселях нужно получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Цвет каждого квадрата необходимо запросить после введения размера квадрата в корректном виде.
 *
 * Итого последовательность ввода данных о квадратах выглядит следующим образом:
 * - Размер квадрата n;
 * - Цвет квадрата n;
 * - Размер квадрата n + 1;
 * - Цвет квадрата n + 1.
 * - Размер квадрата n + 2;
 * - Цвет квадрата n + 3;
 * - и так далее...
 *
 * Если не любом этапе сбора данных о квадратах пользователь кликнул по кнопке «Отмена»,
 * необходимо остановить процесс создания квадратов и вывести в консоль сообщение:
 * «Операция прервана пользователем.».
 *
 * Все стили для каждого квадрата задать через JavaScript за раз.
 *
 * Тип элемента, описывающего каждый квадрат — div.
 * Задать ново-созданным элементам CSS-классы: .square-1, .square-2, .square-3 и так далее.
 *
 * Все квадраты необходимо сделать потомками body документа.
 */

let numberOfSquares;

do {

	numberOfSquares = prompt('Enter the quantity of squares', 'Max 10 squares');

	if (numberOfSquares === null) break;

	if (numberOfSquares < 1 || numberOfSquares > 10) {

		do {

			numberOfSquares = prompt('Noncortical data. Try again', 'Max 10 squares');

			if (numberOfSquares === null) break;

		} while (numberOfSquares < 1 || numberOfSquares > 10 || isNaN(numberOfSquares) || numberOfSquares === '')

	}

} while (isNaN(numberOfSquares) || numberOfSquares === '');

function createSquares(numberOfSquares) {

	let color;
	let size;
	let maxSum = 1;

	do {

		size = prompt('Enter the size of a square');

		if (size === null) {
			console.log('Operation aborted by user');
			break;
		}

		color = prompt('Enter the color of a square');

		if (color === null) {
			console.log('Operation aborted by user');
			break;
		}

		const squareElem = document.createElement('div');

		squareElem.classList.add(`square-${maxSum}`);

		squareElem.style.cssText = `width:${size}; height:${size}; background-color:${color}`;

		document.body.append(squareElem);

		maxSum++;

	} while (maxSum < numberOfSquares)

}

createSquares(numberOfSquares);

