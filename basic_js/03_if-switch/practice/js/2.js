/**
 * Задание 2.
 *
 * Написать программу, которая будет приветствовать пользователя.
 * Сперва пользователь вводит своё имя, после чего программа выводит в консоль сообщение с учётом
 * его должности.
 *
 * Список должностей:
 * Mike — CEO;
 * Jane — CTO;
 * Walter — программист:
 * Oliver — менеджер;
 * John — уборщик.
 *
 * Если введёно не известное программе имя — вывести в консоль сообщение «Пользователь не найден.».
 *
 * Выполнить задачу в двух вариантах:
 * - используя конструкцию if/else if/else;
 * - используя конструкцию switch.
 */

/* if/else if/else */

const userName = prompt("Enter Name");
const userNameLower = userName.toLowerCase();

if (userNameLower === "mike") {
	console.log("Hello, CEO," + userName);
} else if (userNameLower === "jane") {
	console.log("Hello, CTO" + userName);
} else if (userNameLower === "walter") {
	console.log("Hello, Programmer" + userName);
} else if (userNameLower === "oliver") {
	console.log("Hello, Manager" + userName);
} else if (userNameLower === "john") {
	console.log("Hello, Cleaner" + userName);
} else {
	console.log("User Unknown");
}

/* switch */
// const userName = prompt('Введите имя пользователя');
// let pos = ''

// switch (userName.toLowerCase()) {
//     case 'mike':
//         pos = 'CEO';
//         console.log(`Привет ${userName} - ${pos}`);
//         break;
//     case 'jane':
//         pos = 'CTO';
//         console.log(`Привет ${userName} - ${pos}`);
//         break;
//     case 'walter':
//         pos = 'Программист';
//         console.log(`Привет ${userName} - ${pos}`);
//         break;
//     case 'oliver':
//         pos = 'Менеджер';
//         console.log(`Привет ${userName} - ${pos}`);
//         break;
//     case 'john':
//         pos = 'Уборщик';
//         console.log(`Привет ${userName} - ${pos}`);
//         break;
//     default:
//         pos = 'Пользовтель не найден';
//         console.log(pos);
// }
