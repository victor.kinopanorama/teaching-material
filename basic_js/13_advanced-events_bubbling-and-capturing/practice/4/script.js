/**
 * Дан список сообщений с кнопками для удаления [x]. Заставьте кнопки работать.
 * 
 * Используйте делегирование событий. Должен быть лишь один обработчик на элементе-контейнере для всего.
 */

const container = document.querySelector('#container');

container.addEventListener('click', e => {

	const target = e.target;

	if (target.tagName !== 'BUTTON') return;

	target.closest('div .pane').remove();

})