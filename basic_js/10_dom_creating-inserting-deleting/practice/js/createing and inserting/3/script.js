/**
 * Задание 3.
 *
 * Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
 *
 * Цвет тёмных ячеек — #161619.
 * Цвет светлых ячеек — #FFFFFF.
 * Остальные стили CSS для доски и ячеек готовы.
 *
 * Доску необходимо развернуть внутри элемента с классом .board.
 *
 * Каждая ячейка доски представляет элемент div с классом .cell.
 */

/* Дано */
const LIGHT_CELL = '#ffffff';
const DARK_CELL = '#161619';
const V_CELLS = 8;
const H_CELLS = 8;

/* Решение */

const board = document.createElement('section');

board.classList.add('board');

document.body.prepend(board);

const fillChessBoard = () => {

	for (let i = 0; i < V_CELLS; i++) {

		if (i % 2 === 0) {

			let previousCell = DARK_CELL;

			for (let i = 0; i < H_CELLS; i++) {

				const cell = document.createElement('div');
				cell.classList.add('cell')

				if (previousCell === DARK_CELL) {

					cell.style.backgroundColor = LIGHT_CELL;
					board.append(cell);

					previousCell = LIGHT_CELL;

				} else {

					cell.style.backgroundColor = DARK_CELL;
					board.append(cell);

					previousCell = DARK_CELL;

				}

			}

		} else {

			let previousCell = LIGHT_CELL;

			for (let i = 0; i < H_CELLS; i++) {

				const cell = document.createElement('div');
				cell.classList.add('cell')

				if (previousCell === LIGHT_CELL) {

					cell.style.backgroundColor = DARK_CELL;
					board.append(cell);

					previousCell = DARK_CELL;

				} else {

					cell.style.backgroundColor = LIGHT_CELL;
					board.append(cell);

					previousCell = LIGHT_CELL;

				}

			}

		}

	}

}

fillChessBoard();