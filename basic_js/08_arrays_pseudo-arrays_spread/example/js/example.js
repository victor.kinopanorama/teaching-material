const title = document.querySelector('.title');
const img = document.querySelector('.img');

title.addEventListener('click', e => {
	title.classList.toggle('text-blur');
})

img.addEventListener('click', () => {
	img.classList.toggle('img-scale');
})

const person = [
	{ name: 'Viktor', age: 35, budget: 1270000 },
	{ name: 'Brad', age: 27, budget: 40000 },
	{ name: 'Adrew', age: 32, budget: 12000 },
	{ name: 'Mark', age: 39, budget: 1120000 },
	{ name: 'Anastasia', age: 22, budget: 31300 },
	{ name: 'Anna', age: 24, budget: 75000 },
];

let arr = [1, 2, 3, 4, 5];

let studentList = [
	{ name: 'John', gender: 'male', birthDay: '1997-7-4' },
	{ name: 'Sarah', gender: 'female', birthDay: '1992-8-17' },
	{ name: 'Sam', gender: 'male', birthDay: '1987-4-12' },
	{ name: 'Stephen', gender: 'male', birthDay: '2001-8-1' },
	{ name: 'Natalie', gender: 'female', birthDay: '1993-3-20' },
	{ name: 'Brad', gender: 'male', birthDay: '1991-2-14' },
	{ name: 'Emma', gender: 'female', birthDay: '2002-3-6' },
	{ name: 'Roberto', gender: 'male', birthDay: '1985-5-7' },
	{ name: 'Viktor', gender: 'male', birthDay: '1989-1-15' },
	{ name: 'Antonio', gender: 'male', birthDay: '1971-2-11' },
]