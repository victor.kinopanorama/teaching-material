/**
 * Задача 1.
 *
 * Дан массив с днями недели.
 *
 * Вывести в консоль все дни недели тремя способами. С помощью:
 * - Классического цикла for;
 * - Цикла for...of;
 * - Специализированного метода для перебора элементов массива forEach.
 * 
 */

const days = [
	'Monday',
	'Tuesday',
	'Wednesday',
	'Thursday',
	'Friday',
	'Saturday',
	'Sunday',
];

for (let i = 0; i < days.length; i++) {
	console.log(days[i]);
};

for (let e of days) {
	console.log(e)
};

days.forEach(e => console.log(e));


