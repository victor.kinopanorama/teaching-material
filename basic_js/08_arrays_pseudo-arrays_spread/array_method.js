// /**

const days = [
	'Monday',
	'Tuesday',
	'Wednesday',
	'Thursday',
	'Friday',
	'Saturday',
	'Sunday',
];

// * push() – Позволяет добавить один, или более элементов в конец массива.

// * pop() – Позволяет удалить последний элемент из массива и возвратить его значение.

// * shift() – Позволяет удалить первый элемент из массива и возвратить его значение.

// * unshift() - Позволяет добавить один, или более элементов в начало массива.

// * splice() - Позволяет изменить содержимое массива за счёт удаления существующих элементов, и/или добавления новых элементов в массив.

// days.splice(start)
// days.splice(start, deleteCount)
// days.splice(start, deleteCount, elem1, elem2)
// const deletedElems = days.splice(0, 2)
// console.log(days)
// console.log(deletedElems)

// * slice() - Позволяет возвратить новый массив, который содержит копии элементов, вырезанных из исходного массива.

// const newArr = days.slice();
// const newArr2 = days.slice(0, 3);

// * concat() - Метод используется для объединения двух, или более массивов в один, при этом он не изменяет существующие массивы, а возвращает новый массив/

// const newArray = array.concat(array, array1);

//  * forEach
//  * Позволяет выполнить переданную функцию один раз для каждого элемента в массиве
//  arr.forEach(function(item, index, array) {
//  ... do something with item });


// * indexOf(item, from = 0)
// * Метод возвращает индекс искомого элемента в массиве при первом совпадении, или -1 если элемент не найден
// console.log(days.indexOf('January'));

// if (days.indexOf('January') < 0) {
//    alert('Нет такого елемента')
// } else {
//    alert('Елемент найден')
// }

// * lastIndexOf(item, from) Ищет начиная с конца массива
// console.log(days.lastIndexOf('January'));

// * includes(item, from)
// * Позволяет определить, содержит ли массив искомый элемент. В случае нахождения элемента метод возвращает true, в обратном случае false.

// console.log(days.includes('Monday')); // true
// console.log(days.includes('January')); // false

// if (array.includes('Bruce Banner')) {
//    ...do something
// }


// * find() метод возвращает значение первого элемента в массиве, который соответствует условию в переданной функции, или undefined
// console.log(array.find((item) => item.place === 'New York'));
// console.log(array.find((item) => item.place === 'Kyiv'));

// * findIndex() метод возвращает значение индекса элемента в массиве, соответствуещего условию в переданной функции, или -1, если ни один elem не удовлетворяет условию
// console.log(array.findIndex((item) => item.place === 'Moscow'));
// console.log(array.findIndex((item) => item.place === 'Kyiv'));

// * filter(function(item, index, array))
// * Метод позволяет создать новый массив, элементы которого соответствуют условию заданному в пререданной функции

// const filteredArr = days.filter(e => e.length > 5)
// console.log(filteredArr)

//  * split('separator')
// * разбивает строку на єлементы массива

// let str = 'some string';
// let strArr = str.split(' ');
// console.log(strArr);

// * join(separator)
// * обьденяет елементы массива в строку

// let strFromArr = days.join('-');

//  * reverse
//  * разворачивает массив, первый элемент массива становится последним, а последний элемент первым

// const array = [1, 2, 3, 4, 5, 6];
// const reversedArray = array.reverse();

//  * map
//  * позволяет вызвать переданную функцию один раз для каждого элемента массива, формируя новый массив из результатов вызова этой функции
//  * map((element, index, array) => { ... } );
//  */

// const array = ['Thor Odinson', 'Bruce Banner', 'Natasha Romanoff', 'Tony Stark'];

// const newArray = array.map((elem, index) => {
//    const nameArray = elem.split(' ');

//    return {
//       firstName: nameArray[0],
//       lastName: nameArray[1],
//       id: index
//    }
// })

// console.log(newArray);


// const newArray = array.map((elem, index) => {
//     const nameArray = elem.split(' ');
//     return `
//         <span>
//             ${elem}
//         </span>
//     `;
// });

// console.log('newArray: ', newArray.join(' '));

//  * sort
//  * позволяет отсортировать массив путём преобразования его элементов в строки и сравнения этих строк в порядке следования кодовых символов
//  * sort((firstEl, secondEl) => { ... } )
//  */


// const array = [1, 6, 54, 3, 323, 54, 2, 23];

// const callback = (a, b) => {
//     if (a < b) return -1
//     else if (a == b) return 0
//     else if (a > b) return 1
// }

// const sortedArray = array.sort(callback);

// const sortedArray = array.sort((a, b) => b - a);

// console.log('sortedArray: ', sortedArray);
// console.log('array: ', array);

// const stringArray = ['Thor Odinson', 'Bruce Banner', 'Natasha Romanoff', 'Tony Stark'];
// const lettersArray = ['b', 'B', 'a', 'c', 'e', 'd', 'f', 'A', 'C', 'F', 'ab', 'cd', 'Ben Jhonson', 'Don\'t lose yourself, don\'t lose who you really are and what really matters to you!', 'aaa', 'bbb', 'aAb'];

// console.log(lettersArray.sort((a, b) => a < b ? -1 : 1));
// console.log(lettersArray.sort((a, b) => a.localeCompare(b)));
// console.log(lettersArray.sort((a, b) => b.localeCompare(a)));
