/**
 * Задание 3
 * 
 */

const autoSlider = {
	images: [
		'img/1.jpg',
		'img/2.jpg',
		'img/3.jpg',
		'img/4.jpg',
		'img/5.jpg',
	],
	titles: [
		'The greatest glory',
		'Your time is limited',
		'If life were predictable',
		'If you set your goals',
		'Life is what happens',
	],

	currentSlide: 0,
	container: document.querySelector('.container'),
	imageElem: document.querySelector('.image-container img'),
	textElem: document.querySelector('.text-container'),

	start() {
		if (this.currentSlide < this.images.length - 1) {
			setInterval(this.change.bind(this), 1000)
		}
	},

	change() {

		this.currentSlide++;
		this.imageElem.src = this.images[this.currentSlide];
		this.textElem.innerText = this.titles[this.currentSlide];

	}

};

autoSlider.change();
autoSlider.start()
