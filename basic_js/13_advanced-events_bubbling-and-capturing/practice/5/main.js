/**
 * 
 * При нажатии на кнопку закрытия скрывать модальное окно.
 * При этом alert не должен отрабатывать.
 * 
 */

// Dont touch this code
const modalElem = document.querySelector('.modal');
modalElem.addEventListener('click', () => alert('modal closed'));

// Write your own code below

const closeBtn = document.querySelector('.modal__close-btn');
closeBtn.addEventListener('click', e => {

	// останавливает всплытие
	e.stopPropagation();

	modalElem.style.display = 'none';

})
