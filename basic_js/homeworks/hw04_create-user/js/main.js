/**
 * 
 * Метод это функция реализованная в объекте.
 * 
 */

const createNewUser = () => {
   
   const firstName = prompt('Enter your first name');
   const lastName = prompt('Enter your last name');

   const newUser = {
      firstName,
      lastName,
      getLogin() {
         return firstName[0].toLowerCase() + lastName.toLowerCase();
      },
   }

   return newUser;
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin());
