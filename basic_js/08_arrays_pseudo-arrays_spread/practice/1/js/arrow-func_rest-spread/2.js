/**
 * Задание 2.
 *
 * Написать функцию-сумматор всех своих параметров.
 *
 * Функция принимает произвольное количество параметров.
 * Однако каждый из них обязательно должен быть числом. '5' => не принимаем
 *
 * Генерировать ошибку, если:
 * - Если функция была вызвана менее, чем с двумя аргументами;
 * - Хоть один из аргументов не является допустимым числом (в ошибке указать
 * его порядковый номер).
 *
 * Условия:
 * - Использовать тип функции arrow form;
 * - Использовать объект arguments запрещено.
 */

const getParametersSum = (...args) => {

	if (args.length < 2) throw new Error('Less than two arguments entered');

	let result = 0;

	args.forEach((e, ind) => {

		(typeof e === 'number') ? result += e : console.log(`argument under index ${ind} is not a valid number`);

	})

	return result

}

console.log(getParametersSum(1, 2, '5', 64, 137));

// //* Способ с помощью метода Reduce

// const getParametersSum = (...args) => {

// 	if (args.length < 2) throw new Error('Less than two arguments entered');

// 	return args.reduce((total, elem, index) => {

// 		if (typeof elem !== 'number') {

// 			throw new Error(`argument under index ${index} is not a valid number`);

// 		} else {

// 			return total + elem;

// 		}

// 	}, 0)

// }

// console.log(getParametersSum(1, 2, 64, 137));