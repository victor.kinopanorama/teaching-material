/**
 * Написать функцию, которая:
 * - принимает дату в виде строки в формате 'yyyy-mm-dd'
 * - вернет true если день является выходным
 * - вернет false если день является будним днем
 */

const isWeekend = date => {

	const userDate = new Date(date);
	const day = userDate.getDay();

	return (day === 0 || day === 6) ? true : false;

}

console.log(isWeekend('2021-07-02')); // false
console.log(isWeekend('2021-07-03')); // true
console.log(isWeekend('2021-07-04')); // true
console.log(isWeekend('2021-07-05')); // false
console.log(isWeekend('2021-07-06')); // false
