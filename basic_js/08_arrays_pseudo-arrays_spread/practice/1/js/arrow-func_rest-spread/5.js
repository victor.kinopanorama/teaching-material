/**
 * Задание 5.
 *
 * Написать калькулятор на функциях.
 *
 * Программа должна выполнять четыре математические операции:
 * - Сложение (add);
 * - Вычитание (subtract);
 * - Умножение (multiply);
 * - Деление (divide).
 *
 * Каждую математическую операцию должна выполнять отдельная функция.
 * Каждая такая функция обладает двумя параметрами-операндами,
 * и возвращает результат нужной операции над ними.
 *
 * Эти вспомогательный функции использует главная функция calculate,
 * которая обладает тремя параметрами:
 * - Первый — числовой тип, первый операнд;
 * - Второй — числовой тип, второй операнд;
 * - Третий — функцию, ссылка на ранее созданную вспомогательную функцию.
 *
 * Условия:
 * - Никаких проверок типов данных совершать не нужно;
 * - Обязательно использовать паттерн «callback».
 */

const addNumbers = (a, b) => a + b;
const subtractNumbers = (a, b) => a - b;
const multiplyNumbers = (a, b) => a * b;
const divideNumbers = (a, b) => a / b;

const calculate = (num1, num2, operation) => {

	return operation(num1, num2)

}

console.log(calculate(5, 9, addNumbers));
console.log(calculate(1, 4, subtractNumbers));
console.log(calculate(3, 12, multiplyNumbers));
console.log(calculate(14, 7, divideNumbers));
