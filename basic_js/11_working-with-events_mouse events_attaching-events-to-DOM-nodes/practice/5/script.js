/**
* Початкове значення кнопки повинно дорівнювати 0
* При натисканні на кнопку збільшувати це значення на 1
*
*/

const button = document.querySelector('.counter');

const increment = e => {

	let target = e.target;

	if (e.ctrlKey) {

		if (target.innerText > 0) {

			target.innerText--;

		}

	} else {

		target.innerText++;

	}

};

button.addEventListener('click', increment);