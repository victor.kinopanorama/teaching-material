//* ------------- Функции -------------

// Функции придуманы, чтобы не повторять один и тот же код во многих местах

//* Function Declaration (Объявление Функции)

// Функции, созданные, как Function Declaration могут быть вызваны раньше своих определений.

// sayHi("Вася");

// function sayHi(name) {
//   alert( `Привет, ${name}` );
// }

//* Function Expression (Функциональное Выражение)
// const greet2 = function greet2(аргументы) {
//   тело функции;
// }

// greet2(значения)

// console.log(typeof greet)
// console.dir(greet)

// Переменные, объявленные внутри функции, видны только внутри этой функции
// Глобальная переменная используется, только если внутри функции нет такой локальной
// Scope - область видимости

// Директива return может находиться в любом месте тела функции. Как только выполнение доходит до этого места, функция останавливается, и значение возвращается
// Возможно использовать return и без значения. Это приведёт к немедленному выходу из функции

//* Выбор имени функции
// "get…" – возвращают значение,
// "calc…" – что-то вычисляют,
// "create…" – что-то создают,
// "check…" – что-то проверяют

//* Параметры по дефолту
// Если параметр не передан, то его значением становится undefined
// function showName(name = "текст не добавлен")

// 2 Анонимные функции
// let counter = 0
// const interval = setInterval(function() {
//   if (counter === 5) {
//     clearInterval(interval) // clearTimeout
//   } else {
//     console.log(++counter)
//   }
// }, 1000)

//* Функции callback
// Мы передаём функцию в качестве аргумента и ожидаем, что она вызовется обратно когда понадобится.

// function ask(question, yes, no) {
//    if (confirm(question)) yes()
//    else no();
// }

// function showConfirm() {
//    alert("Confirmed!");
// }

// function showCancel() {
//    alert("Canceled!");
// }

// // функции showConfirm, showCancel передаются в качестве аргументов ask
// ask("Do you agree?", showConfirm, showCancel);

//* Анонимные функции

// Анонимная - функция без имени, которую мы передали колбэком в setInterval
// let counter = 0;

// const interval = setInterval(function () {
//    if (counter === 5) {
//       // clearInterval останавливает выполнение функции
//       clearInterval(interval);
//    } else {
//       console.log(counter++);
//    }
// }, 1000);


//* Стрелочные функции (arrow function)

// Стрелочные функции не могут быть вызваны раньше своих определений!

//Если только один аргумент, то круглые скобки вокруг параметров можно опустить
// const arrow = (name, age) => {
//    console.log('Привет - ', name, age)
// }

// const arrow2 = name => console.log('Привет - ', name)

// arrow2('Viktor')

// const pow = num => num ** 2

// console.log(pow(5))

// Параметры по умолчанию
// const sum = (a = 40, b = a * 2) => a + b

// console.log(sum(41, 4))
// console.log(sum())

// function sumAll(...all) {
//    let result = 0
//    for (let num of all) {
//       result += num
//    }
//    return result
// }

// const res = sumAll(1, 2, 3, 4, 5)
// console.log(res)

//* 5 Замыкания

// Из одной функции возвращаем другую функцию
// function createUser(name) {
//    return function (lastName) {
//       console.log(name + lastName)
//    }
// }

const userWithLastName = createUser('Viktor')
// теперь функция userWithLastName постоянно будет работать с name = Viktor (name замкнулось)
console.log(userWithLastName('Kostenko'))
console.log(userWithLastName('Coder'))
