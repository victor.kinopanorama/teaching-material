const mainTitle = document.querySelector('.main__title');
const mainText = document.querySelector('.main__text');
const buttonsContainer = document.querySelector('.main__buttons');
const buttons = document.querySelectorAll('button');

const mainContainer = document.querySelector('.container');

const attr = {}

if (localStorage.getItem('attributes') !== null) {

	const localAttr = localStorage.getItem('attributes');
	const parseLocalAttr = JSON.parse(localAttr);

	mainContainer.style.backgroundColor = `${parseLocalAttr.backgroundColor}`;
	mainText.style.color = `${parseLocalAttr.color}`;
	mainTitle.style.color = `${parseLocalAttr.color}`;

	buttons.forEach(e => e.style.cssText = `color: ${parseLocalAttr.color}; border: 2px solid ${parseLocalAttr.color}; background-color: ${parseLocalAttr.backgroundColor};`)

}

// buttonsContainer.addEventListener('click', e => {

// 	const target = e.target;

// 	if (target.tagName !== 'BUTTON') return;

// 	if (target.className === 'buttons__btn-black') {

// 		mainContainer.style.backgroundColor = 'black';
// 		mainText.style.color = 'white';
// 		mainTitle.style.color = 'white';

// 		buttons.forEach(e => e.style.cssText = 'color: white; border: 2px solid white; background-color: black;')

// 		localStorage.setItem('bgcolor', 'black');
// 		localStorage.setItem('color', 'white');
// 	} else {
// 		mainContainer.style.backgroundColor = 'white';
// 		mainText.style.color = 'black';
// 		mainTitle.style.color = 'black';

// 		buttons.forEach(e => e.style.cssText = 'color: black; border: 2px solid black; background-color: white;')

// 		localStorage.setItem('bgcolor', 'white');
// 		localStorage.setItem('color', 'black');
// 	}

// })

buttonsContainer.addEventListener('click', e => {

	const target = e.target;

	if (target.tagName !== 'BUTTON') return;

	if (target.className === 'buttons__btn-black') {

		mainContainer.style.backgroundColor = 'black';
		mainText.style.color = 'white';
		mainTitle.style.color = 'white';

		buttons.forEach(e => e.style.cssText = 'color: white; border: 2px solid white; background-color: black;')

		attr.backgroundColor = 'black';
		attr.color = 'white';
		localStorage.setItem('attributes', JSON.stringify(attr));

	} else {
		mainContainer.style.backgroundColor = 'white';
		mainText.style.color = 'black';
		mainTitle.style.color = 'black';

		buttons.forEach(e => e.style.cssText = 'color: black; border: 2px solid black; background-color: white;')

		attr.backgroundColor = 'white';
		attr.color = 'black';
		localStorage.setItem('attributes', JSON.stringify(attr));
	}

})


