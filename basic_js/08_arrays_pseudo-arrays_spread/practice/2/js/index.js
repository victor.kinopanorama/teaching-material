/**
 * 
 * Дан массив состоящий из объектов сотрудников. Напишите функцию которая будет принимать в себя Имя сотрудника (всегда строка)
 * или его ID (всегда число) и удалять объект указанного сотрудника из массива.
 * 
 * - Второй опциональный аргумент - объект нового сотрудника. Если второй аргумент передан то функция
 * должна вставить в массив новый объект вместо старого;
 * - Функция должна возвращать объект удаленного сотрудника;
 * - Если в массиве нет такого сотрудника функция должна вывести alert "Сотрудник не найден" и вернуть null;
 * 
 * - Используйте метод splice();
 * 
**/

const employees = [
	{
		name: 'Brandy Hambleton',
		age: 31,
		position: 'designer',
		id: 132,
	},
	{
		name: 'Killa Shikoba',
		age: 25,
		position: 'QA',
		id: 12,
	},
	{
		name: 'Colten Walsh',
		age: 46,
		position: 'lead developer',
		id: 32,
	},
	{
		name: 'Stace Rounds',
		age: 16,
		position: 'intern',
		id: 11,
	},
	{
		name: 'Joel James',
		age: 21,
		position: 'key developer',
		id: 54,
	},
	{
		name: 'Brannon Duke',
		age: 22,
		position: 'key developer',
		id: 56,
	},
];

//* Вариант 1

function getEmployee(value, newEmployee = '') {

	let deletedEmployee;
	let res = false;

	if (typeof value === 'string') {

		employees.forEach((el, ind) => {

			if (el.name === value) {

				res = true;

				if (newEmployee !== '') {

					deletedEmployee = el;
					employees.splice(ind, 1, newEmployee);

				} else {

					deletedEmployee = el;
					employees.splice(ind, 1);

				}

			};

		});

	} else if (typeof value === 'number') {

		employees.forEach((el, ind) => {

			if (el.id === value) {

				res = true;

				if (newEmployee !== '') {

					deletedEmployee = el;
					employees.splice(ind, 1, newEmployee);

				} else {

					deletedEmployee = el;
					employees.splice(ind, 1);

				}

			};

		})

	}

	if (res) {

		return deletedEmployee;

	} else {

		alert('Сотрудник не найден');
		return null;

	}

}

const newEmployee = {
	name: 'Viktor',
	age: 34,
	position: 'Seniour Architect',
	id: 322,
}

const deletedEmployee = getEmployee('Joel James', newEmployee);
console.log(deletedEmployee);
console.log(employees);

//* Вариант 2

// function removeEmployee() {

// 	let deletedEmployee;
// 	let isFind = false;

// 	if (typeof arguments[0] === 'number') {
// 		employees.forEach((e, ind) => {

// 			if (e.id === arguments[0]) {

// 				isFind = true;

// 				if (arguments.length <= 1) {

// 					employees.splice(ind, 1)
// 					deletedEmployee = employees[ind];

// 				} else {

// 					employees.splice(ind, 1, arguments[1])
// 					deletedEmployee = employees[ind];
// 				}

// 			}

// 		})
// 	}

// 	if (typeof arguments[0] === 'number') {
// 		employees.forEach((e, ind) => {

// 			if (e.id === arguments[0]) {

// 				isFind = true;

// 				if (arguments.length <= 1) {

// 					employees.splice(ind, 1)
// 					deletedEmployee = employees[ind];

// 				} else {

// 					employees.splice(ind, 1, arguments[1])
// 					deletedEmployee = employees[ind];
// 				}

// 			}

// 		})
// 	}

// 	if (isFind) {

// 		return deletedEmployee;

// 	} else {

// 		console.log('Сотрудник не найден');
// 		return null

// 	}

// }