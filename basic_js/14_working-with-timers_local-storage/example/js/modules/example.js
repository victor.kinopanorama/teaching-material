const array = ['Larry Wils', 'Dreyk Jacson', 'Ouen Wils', 'Mark Zukkerberg'];

const mapedArray = array.map((elem, ind) => {

	const splitedElem = elem.split(' ');

	return ({
		firstName: splitedElem[0],
		lastName: splitedElem[1],
		id: ind
	})

})

console.log(mapedArray);