const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const arr2 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

const list = document.querySelector('.list')

function createListOnPage(arr, container) {

	arr.map(elem => {

		if (Array.isArray(elem)) {

			const innerList = document.createElement('ul');
			container.append(innerList);

			elem.map(innerElem => innerList.insertAdjacentHTML('beforeend', `<li>${innerElem}</li>`))

		} else {

			container.insertAdjacentHTML('beforeend', `<li>${elem}</li>`)

		}

	})

	const listElems = document.querySelectorAll(`.list`);

	let timerSum = 3;

	const timer = setInterval(() => {

		console.log(`${timerSum}`);
		timerSum--;

	}, 1000)

	setTimeout(() => listElems.forEach(e => {

		e.remove()
		clearInterval(timer)

		console.log('Items deleted');

	}), 3000)

}

createListOnPage(arr2, list);