/**
 * Задание 4.
 *
 * Напишите программу «Кофейная машина».
 *
 * Программа принимает монеты и готовит напитки:
 * - Кофе за 25 монет;
 * - Капучино за 50 монет;
 * - Чай за 10 монет.
 *
 * Чтобы программа узнала что делать, она должна знать:
 * - Сколько монет пользователь внёс;
 * - Какой он желает напиток.
 *
 * В зависимости от того, какой напиток выбрал пользователь,
 * программа должна вычислить сдачу и вывести сообщение в консоль:
 * «Ваш «НАЗВАНИЕ НАПИТКА» готов. Возьмите сдачу: «СУММА СДАЧИ».".
 *
 * Если пользователь ввёл сумму без сдачи — вывести сообщение:
 * «Ваш «НАЗВАНИЕ НАПИТКА» готов. Спасибо за сумму без сдачи! :)"
 */

const coffeePrice = 25;
const cappuccinoPrice = 50;
const teaPrice = 10;

const enteredMoney = +prompt('Place money');
const selectedDrink = prompt('Choose a drink');
const selectedDrinkLower = selectedDrink.toLowerCase();

if (isNaN(enteredMoney) || enteredMoney < 0 || selectedDrink === '' || !isNaN(selectedDrink)) {

	console.warn('Incorrect input!');

} else {

	switch (selectedDrinkLower) {

		case 'чай':

			if (enteredMoney === teaPrice) {

				alert(`Ваш ${selectedDrinkLower} готов. Спасибо за сумму без сдачи! :)`)

			} else if (enteredMoney < teaPrice) {

				alert(`Вы ввели недпстаточно средств! Добавьте еще ${teaPrice - enteredMoney} центов.`)

			} else {

				alert(`Ваш ${selectedDrinkLower} готов. Возьмите сдачу: ${enteredMoney - teaPrice}.`)

			}
			break;

		case 'кофе':

			if (enteredMoney === coffeePrice) {
				alert(`Ваш ${selectedDrinkLower} готов. Спасибо за сумму без сдачи! :)`)
			} else if (enteredMoney < coffeePrice) {
				alert(`Вы ввели недпстаточно средств! Добавьте еще ${coffeePrice - enteredMoney} центов.`)
			} else {
				alert(`Ваш ${selectedDrinkLower} готов. Возьмите сдачу: ${enteredMoney - coffeePrice}.`)
			}
			break;

		case 'капучино':
			if (enteredMoney === cappuccinoPrice) {
				alert(`Ваш ${selectedDrinkLower} готов. Спасибо за сумму без сдачи! :)`)
			} else if (enteredMoney < cappuccinoPrice) {
				alert(`Вы ввели недпстаточно средств! Добавьте еще ${cappuccinoPrice - enteredMoney} центов.`)
			} else {
				alert(`Ваш ${selectedDrinkLower} готов. Возьмите сдачу: ${enteredMoney - cappuccinoPrice}.`)
			}
			break;

		default:
			alert('Введите корректное название напитка!')
	}
}

// if(isNaN(enteredMoney) || enteredMoney === '') {
//    console.log('You dont place money');
// } else {
//    if(selectedDrinkLower === 'кофе') {
//       if(enteredMoney === 25) {
//          console.log(`Yours coffee is ready. Thank you for the amount without delivery!:)`);
//       } else if(enteredMoney > 25) {
//          console.log(`Your coffee is ready. Take the delivery: ${enteredMoney - 25}`);
//       } else {
//          console.log('You have not contributed enough money');
//       }
//    } else if(selectedDrinkLower === 'капучино') {
//       if(enteredMoney === 50) {
//          console.log(`Yours cappuccino is ready. Thank you for the amount without delivery!:)`);
//       } else if(enteredMoney > 50) {
//          console.log(`Your cappuccino is ready. Take the delivery: ${enteredMoney - 50}`);
//       } else {
//          console.log('You have not contributed enough money');
//       }
//    } else if(selectedDrinkLower === 'чай') {
//       if(enteredMoney === 10) {
//          console.log(`Yours tea is ready. Thank you for the amount without delivery!:)`);
//       } else if(enteredMoney > 10) {
//          console.log(`Your tea is ready. Take the delivery: ${enteredMoney - 10}`);
//       } else {
//          console.log('You have not contributed enough money');
//       }
//    }
// }