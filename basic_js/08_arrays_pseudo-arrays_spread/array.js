//* ---------- Массивы ----------

// Массивы(arrays) - используют для хранения упорядоченных коллекций
// Массивы копируются по ссылке

// Объявление
// let arr = new Array();
// let arr = [];

// В массиве могут храниться разные типы значений
let arr = ['Яблоко', { name: 'Джон' }, true, function () { alert('привет'); }];

// получить элемент с индексом 3 (функция) и выполнить её
// arr[3]();

const cars = ['Mazda', 'Ford', 'BMW', 'Ferarri', 'Audi'];

// console.log(cars[1]) - выводит элемент по идексу
// cars[1] = 'Toyota' - заменяет элемент по индексу

// cars.length - длина массива
// cars[cars.length] = 'Renault' - добавит элемент в конец массива

//* Array.from
// Преобразует что либо в массив

let str = 'Hello';

console.log(Array.from(str));

//* Перебор эдементов

// for (let i = 0; i < cars.length; i++) {
//    console.log(cars[i]);
// }

// for (let car of cars) {
//    console.log(car);
// }

//* Методы

//* push - добавить в конец массива
// cars.push('Renault')
//* unshift - добавить в начало массива
// cars.unshift('Toyota')

//* shift - удаляем первый элемент из массива
// const firstCar = cars.shift()
//* pop - удаляем последний элемент из массива
// const lastCar = cars.pop()

//* Переворачивает массив
// console.log(cars.reverse())
// const text = 'Привет, мы изучаем JavaScript'
// const reverseText = text.split('').reverse().join('')
// console.log(reverseText)

//* Выводит индекс по заданному элементу
// const index = cars.indexOf('БМВ')
// cars[index] = 'Porsche'

//* findIndex
// Возвращает значение индекса элемента в массиве, который соответствует условию в переданной функции, или -1, если ни один элемент не удовлетворяет условию

const people = [
   { name: 'Viktor', age: 32 },
   { name: 'Elena', age: 23 },
   { name: 'Alex', age: 41 },
]

// const person = people.findIndex(person => person.age === 41)
// console.log(person); // вернет 2

// const person = people.findIndex(person => person.age === 82)
// console.log(person); // вернет -1

//* find
// Возвращает значение первого элемента в массиве, который соответствует условию в переданной функции, или undefined, если ни один элемент не удовлетворяет условию

// const person = people.find(person => person.age === 32)
// console.log(person); // вернет {name: 'Viktor', age: 32}

//* Object.keys(obj)
// возвращает массив ключей

//* Object.values(obj)
// возвращает массив значений

//* Object.entries(obj)
// возвращает массив пар [ключ, значение]

//* includes
// Позволяет определить, содержит ли массив искомый элемент. Возвращает true или false
// console.log(cars.includes('Ford!')) // true

//* map
// Позволяет вызвать переданную функцию один раз для каждого элемента массива, формируя новый массив из результатов вызова этой функции

// Вернёт новый массив
// const upperCaseCars = cars.map(car => {
//   return car.toUpperCase()
// })
//

//* filter
// Метод позволяет создать новый массив, элементы которого соответствуют условию заданному в пререданной функции

// const filtereArray = people.filter(e => e < 37);
// console.log(filtereArray);


//* reduce
// Перебирает массив и сохраняет промежуточный результат

let numArray = [3, 4, 5, 9, 17];

// let reducedArr = numArray.reduce((prevValue, currentValue, index) => {
//    console.log('prevValue = ', prevValue); // первый элемент (в нем сохрагяется промежуточное значение)
//    console.log('index: ', index);
//    console.log('currentValue = ', currentValue); // каждый последующий элемент

//    return prevValue + currentValue;
//    выведет сумму элементов массива
// })

// console.log(reducedArr);

const millioners = [
   { name: 'Viktor', bank: 7342000 },
   { name: 'Elena', bank: 1230000 },
   { name: 'Alex', bank: 4156000 },
   { name: 'Fox', bank: 3166800 },
]

let allCash = millioners.reduce((acc, current) => {
   return acc + current.bank;
}, 0) // 0 это начальное значение

console.log(allCash + '$');
