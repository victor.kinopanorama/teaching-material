/**
 * Задание 1.
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер и цвет квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в некорректном формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Все стили для квадрата задать через JavaScript посредством одной строки кода.
 *
 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.
 */

const getSize = () => {

	let result;

	do {

		result = +prompt("Enter the size");

		if (result === null) break;

	} while (isNaN(result) || result === '');

	return result + 'px';

}

const createElement = (size, color) => {

	const div = document.createElement('div');

	div.classList.add('square');
	div.style.cssText = `width: ${size}; height: ${size}; background-color: ${color};`;

	document.body.append(div);
}

createElement(getSize(), 'green');