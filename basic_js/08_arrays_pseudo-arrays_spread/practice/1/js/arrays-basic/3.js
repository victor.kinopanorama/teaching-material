/**
 * Задача 3.
 *
 * Напишите функцию mergeArrays для объединения нескольких массивов в один.
 *
 * Функция обладает неограниченным количеством параметров.
 * Функция возвращает один массив, который является сборным из массивов,
 * переданных функции в качестве аргументов при её вызове.
 *
 * Условия:
 * - Все аргументы функции должны обладать типом «массив», иначе
 * генерировать ошибку;
 * - В ошибке обязательно указать какой по счёту аргумент провоцирует ошибку.
 *
 * Заметки:
 * - Делать поддержку выравнивания вложенных массивов (флеттенинг) не нужно.
 */

function mergeArrays(...args) {

	let newArr = []

	args.forEach((arr, index) => {

		if (!Array.isArray(arr)) {

			throw new Error(`Element by index ${index} not an array type`)

		} else {

			for (let elem of arr) newArr.push(elem)

		}

	})

	return newArr;

}

let unitedArray = mergeArrays([1, 2, 'Cors'], [2, 'Trex', 'ex'], ['tex', 78])

console.log(unitedArray);






























// const mergeArrays = (...args) => {
//     let arr = [];
    
//     let index = args.findIndex((el) => !Array.isArray(el)) 

//     if (index === -1){
//     args.forEach((el) => arr.push(...el))
//     return arr;
//     } else {
//         throw new Error (`Ошибка в элементе ${index}`)
//     }
    

// }

// console.log(mergeArrays([1, 2, 3], [4, 5, 6], ['Hello'], [' ', 'world', '!']));
// console.log(mergeArrays([1], 'error!'));
