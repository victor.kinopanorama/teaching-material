/**
 * Задача 2.
 *
 * Написать функцию-исследователь навыков разработчика developerSkillInspector.
 *
 * Функция не обладает аргументами и не возвращает значение.
 * Функция опрашивает пользователя до тех пор, пока не введёт хотя-бы один свой навык.
 *
 * Если пользователь кликает по кнопке «Отменить» в диалоговом окне, программа оканчивает
 * опрос и выводит введённые пользователем навыки в консоль.
 *
 *
 * Условия:
 * - Список навыков хранить в форме массива;
 * - В списке навыков можно хранить только строки длиной один символ или больше.
 */

function developerSkillInspector() {

	let userSkill = prompt('Enter a skill')

	let skillsArr = [];

	while (userSkill || userSkill === '') {

		if (userSkill === null) break;

		if (isNaN(userSkill)) skills.push(userSkill);

		userSkill = prompt('Enter a skill');

	}

	skills.forEach(skill => console.log(skill));

	return skillsArr;

}

developerSkillInspector();