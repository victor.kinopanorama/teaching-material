const logInBtn = document.querySelector('.sign-in__btn');
const inputsElems = document.querySelectorAll('input');
const inputRadioContainer = document.querySelector('.sign-in__radio-wrap');
const checkboxElem = document.querySelector('.sign-in__checkbox-input');

const userInfo = {};

logInBtn.addEventListener('click', event => {

	event.preventDefault();

	if (checkboxElem.checked) {
		userInfo.remember = true;
	} else {
		userInfo.remember = false;
	}

	inputsElems.forEach(elem => {

		if (elem.type === 'text' || elem.type === 'password') {
			userInfo[elem.name] = elem.value;
		}

	})

	localStorage.user = JSON.stringify(userInfo);

});

inputRadioContainer.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName === 'SPAN') {
		userInfo.sex = target.closest('label').innerText;
	}

	if (target.tagName === 'LABEL') {
		userInfo.sex = target.innerText;
	}

	localStorage.user = JSON.stringify(userInfo);
});

console.log(localStorage.user);

console.log(JSON.parse(localStorage.getItem('user')).name);
console.log(JSON.parse(localStorage.getItem('user')).password);
console.log(JSON.parse(localStorage.getItem('user')).remember);
console.log(JSON.parse(localStorage.getItem('user')).sex);

