// Функция выдаёт случайное число от min до (max + 1)
function randomInteger(min, max) {
	let rand = min + Math.random() * (max + 1 - min)

	return Math.floor(rand);
}

// переменную totalDonates выносим за пределы функции, чтобы она каждый раз не перезаписывалась
let totalDonates = 0;

function donateRecursion() {

	let donate = randomInteger(0, 100)

	totalDonates += donate;

	return (totalDonates > 10000) ? totalDonates : donateRecursion();
}

console.log(donateRecursion());

//* Пример с циклом
// let totalDonates = 0;

// function donateCycle() {

// 	while (true) {

// 		let donate = randomInteger(0, 100);

// 		totalDonates += donate;

// 		if (totalDonates > 300) return

// 	}

// }

// donateCycle()

// console.log(totalDonates);