/**
 * Задание 1.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 *
 * Advanced: выводить также комбинации клавиш Shift + key, Alt + key, Ctrl + key
 *
 */


const title = document.createElement('h1');
title.textContent = 'Нажмите любую клавишу';
document.body.append(title);

window.addEventListener('keyup', e => {

	if (e.shiftKey) {

		title.textContent = `Нажатая клавиша: Shift + ${e.key}`

	} else if (e.ctrlKey) {

		title.textContent = `Нажатая клавиша: Ctrl + ${e.key}`

	} else if (e.altKey) {

		title.textContent = `Нажатая клавиша: Alt + ${e.key}`

	} else {

		title.textContent = `Нажатая клавиша: ${e.key}`

	}

});

