//* ----------- String -----------

//* Содержимое строки в JavaScript нельзя изменить. Нельзя взять символ посередине и заменить его. Как только строка создана — она такая навсегда!

const name = 'Владилен';

//* length - свойство, которое содержит длину строки
// console.log(name.length)

//* \n - символ для переноса строки

const str = 'Hello world';
const str2 = 'Name: Viktor'

//* split - позволяет разбить строки на массив подстрок, используя заданную строку разделитель для определения места разбиения
// console.log(str2.split(':')) // ['Name','Viktor']

//* slice - позволяет возвратить новую строку, которая содержит копии символов, вырезанных из исходной строки
// console.log(str.slice( 7, 9 )) // "or"
// console.log(str.slice( -5 )) // "world"

//* replace - удаляет буквы, цифры или пробелы из строки
// str.replace(/[^a-zа-яё]/gi, '') удалит всё кроме букв
// str.replace(/[^0-9]/g, '') удалит всё кроме цифр

//* toUpperCase - символы в верхний регистр
// console.log(name.toUpperCase())
//* toLowerCase - символы в нижний регистр
// console.log(name.toLowerCase())

//* charAt - показывает какой символ по указанному индексу
// console.log(name.charAt(2)) // 'a'

//* includes -проверяет, содержит ли строка заданную подстроку, и возвращает true или false. Вторым аргументом можно казать индекс, с которого начать поиск
// 'Синий кит'.includes('синий'); // вернёт false (чувствителен к регистру)
// console.log(str.includes('кит'));    // true

//* indexOf - показывает с какого индекса начмнается введенная строка или символ (если такого символа нет, тогда возвращает -1)
// console.log(name.indexOf('лен')) // 6

//* startsWith - проверяет начинаеться ли строка с введенного слова
// console.log(name.startsWith('Влад')) // true
//* endsWith - проверяет заканчивается ли строка с введенным словом
// console.log(name.endsWith('ен!')) // false

//* repeat - повторяет строку заданное число раз
// console.log(name.repeat(3)) // ВладиленВладиленВладилен

const string = '     password      '

//* trim - метод, который очищает пробелы с обоих боков
// console.log(string.trim())
//* trimLeft - метод, который очищает пробелы слева
// console.log(string.trimLeft())
//* trimRight - метод, который очищает пробелы справа
// console.log(string.trimRight())

function logPerson(s, name, age) {
	if (age < 0) {
		age = 'Еще не родился'
	}
	return `${s[0]}${name}${s[1]}${age}${s[2]}`
}

const personName = 'Владилен'
const personName2 = 'Максим'
const personAge = 26
const personAge2 = -10

// Вновом синтаксисе можно передать шаблонную строку вместо аргументов. Первым аргументом будет идти массив строк - ['Имя: ', 'Возраст: ',], а потом динамически переданные аргументы
const output = logPerson`Имя: ${personName}, Возраст: ${personAge}!`
const output2 = logPerson`Имя: ${personName2}, Возраст: ${personAge2}!`

console.log(output)
console.log(output2)
