/**
 * 
 * Геттеры и Сеттеры (get & set)
 * 
 * Свойства-аксессоры представлены методами: «геттер» – для чтения и «сеттер» – для записи. 
 * При литеральном объявлении объекта они обозначаются get и set.
 * 
 */

let obj = {
	get propName() {
		// геттер, срабатывает при чтении obj.propName
	},

	set propName(value) {
		// сеттер, срабатывает при записи obj.propName = value
	}
};

let user = {
	name: 'Viktor',
	lastName: 'Kostenko',
	age: 35,

	get fullName() {
		return `${this.name} ${this.lastName}`
	},

	set fullName(value) {
		[this.name, this.lastName] = value.split(' ');
	}
}

console.log(user.fullName);
// Мы не вызываем user.fullName как функцию, а читаем как обычное свойство: геттер выполнит всю работу за кулисами.

// set fullName запустится с данным значением
user.fullName = "Alice Cooper";

console.log(user.name);
console.log(user.lastName);

//* Object.defineProperty()
// Статический метод Object.defineProperty() определяет новое или изменяет существующее свойство объекта и возвращает этот объект.

const car = {};

Object.defineProperty(car, 'model', {
	value: 'BMW',
	// writable: false,
});

console.log(car);

car.model = 'Jeep';

delete car.model;

console.log(car);

//* Object.create()
// Этот обьект нельзя будет перебирать

// enumerable, writable, configurable - это дескрипторы
// по дефолту все значения дескрипторов - false

const car = Object.create({
	calculateAge() {
		console.log(new Date().getFullYear - this.year);
	}
},
	{
		model: {
			value: 'Bentley',
			enumerable: true, // можно ли перебрать значения
			writable: true, // можно ли изменять значения
			configurable: true // можно ли удалять значения
		},
		year: {
			value: 2021
		},
		age: {
			get() {
				return new Date().getFullYear - this.year;
			},
			set(value) {
				console.log('set age', value);
			}
		}
	})