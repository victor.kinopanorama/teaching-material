/**
 * Задание 3.
 *
 * Получить элементы с классом .list-item.
 * Отобрать элемент с контентом: «Item 5».
 *
 * Заменить *текстовое* содержимое этого элемента на указанную ссылку.
 *
 * Сделать это так, чтобы новый элемент в разметке не был создан.
 *
 * Затем отобрать элемент с контентом: «Item 6».
 * Заменить содержимое (HTML) этого элемента на такую-же ссылку.
 *
 * Сделать это так, чтобы в разметке был создан новый элемент.
 *
 * Условия:
 * - Обязательно использовать метод для перебора;
 * - Объяснить разницу между типом коллекций: Array и NodeList.
 */

const linkText =
	'<a href="https://www.google.com" target="_blank" rel="noreferrer noopener">Google it!</a>';

// //* Решение

// const collection = document.querySelectorAll('.list-item');

// const item5 = Array.from(collection).find(elem => elem.innerText === 'Item 5');

// if (item5) {
// 	item5.innerText = linkText;
// }

// for (const elem of collection) {
// 	if (elem.innerHTML === 'Item 6') {
// 		elem.innerHTML = linkText;
// 		break;
// 	}
// }