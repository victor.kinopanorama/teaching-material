/**
 * Создать следующих пользователей:
 * Ryan Hansen, 34
 * Linda Daniels, 27
 * Samuel Sims, 24
 * 
 * Вывести каждого пользователя в консоль.
 * 
 * Написать функцию, которая будет создавать объекты пользователей. 
 */

function createUser(user, age) {

	const splitedUser = user.split(' ')

	return {
		firstName: splitedUser[0],
		lastName: splitedUser[1],
		age
	}

}

const user1 = createUser('Ryan Hansen', 34);
const user2 = createUser('Linda Daniels', 27);
const user3 = createUser('Samuel Sims', 24);

console.log(user1);
console.log(user2);
console.log(user3);