const navList = document.querySelector('.our-services__nav-list');
let currentTarget;

navList.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName !== 'LI') return;

	currentTarget = target.dataset.category;

	const navListItems = document.querySelectorAll('.our-services__nav-list-item');
	const servicesContentItem = document.querySelectorAll('.our-services__content-item');

	navListItems.forEach(item => item.classList.remove('active'));
	servicesContentItem.forEach(item => item.classList.remove('visible'));

	servicesContentItem.forEach(item => {

		if (currentTarget === item.dataset.category) {

			target.classList.add('active');
			item.classList.add('visible');

		}

	})
});