/**
 * При натисканні на кнопку Validate відображати
 * VALID зеленим кольром, якщо значення проходить валідацію
 * INVALID червоним кольором, якщо значення не проходить валідацію
 *
 * Правила валідації значення:
 * - значення не пусте
 *
 * ADVANCED
 * Правила валідації значення:
 * - повинно містити щонайменше 5 символів
 * - не повинно містити пробілів
 *
 */

const validateBtn = document.querySelector('#validate-btn');
const message = document.createElement('p');
const inputElem = document.querySelector('#input');


validateBtn.addEventListener('click', () => {

	const inputValue = inputElem.value;

	message.remove()

	if (inputValue !== '' && inputValue.length < 5 && !inputValue.includes(' ')) {

		message.textContent = 'VALID';
		message.style.color = 'green';

		document.body.append(message)

	} else {

		message.textContent = 'INVALID';
		message.style.color = 'red';

		document.body.append(message)

	}

})
