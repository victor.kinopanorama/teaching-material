import './modules/_example.js'

import './modules/_hero.js'
import './modules/_modal.js'
import './modules/_coords.js'
import './modules/_squares.js'
import './modules/_time.js'