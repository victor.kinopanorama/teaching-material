/**
 * Задача 1.
 *
 * Необходимо «оживить» навигационное меню с помощью JavaScript.
 *
 * При клике на элемент меню добавлять к нему CSS-класс .active.
 * Если такой класс уже существует на другом элементе меню, необходимо
 * с того, предыдущего элемента CSS-класс .active снять.
 *
 * У каждый элемент меню — это ссылка, ведущая на google.
 * С помощью JavaScript необходимо предотвратить переход по
 * всем ссылка на этот внешний ресурс.
 *
 * Условия:
 * - В реализации обязательно использовать приём делегирования событий
 * (на весь скрипт слушатель должен быть один).
 */

const menu = document.querySelector('.menu');
const menuLinks = document.querySelectorAll('a');

menu.addEventListener('click', e => {

	e.preventDefault();
	const target = e.target;

	menuLinks.forEach(link => link.classList.remove('active'));

	if (target.tagName !== 'A') return;

	target.classList.add('active');

})

