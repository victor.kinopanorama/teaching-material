//Функции нужны, чтобы не повторять одинаковый код в разных местах, а просто вызывать функцию с прописанным в неё кодом
//Аргументы передаются в функцию и присваиваются её параметрам для работы с ними внутри функции

let firstNum;
let secondNum;

do {
   firstNum = +prompt('Enter first number', firstNum);
} while (firstNum === '' || isNaN(firstNum));

do {
   secondNum = +prompt('Enter second number', secondNum);
} while (secondNum === '' || isNaN(secondNum));

let mathOperation = prompt('Enter operator');

function calc(firstNum, secondNum, mathOperation) {
   let summ;
   
   switch (mathOperation) {
      case '+':
         summ = firstNum + secondNum;
         break;
      case '-':
         summ = firstNum - secondNum;
         break;
      case '*':
         summ = firstNum * secondNum;
         break;
      case '/':
         summ = firstNum / secondNum;
         break;
   }

   return summ;
}

console.log(calc(firstNum, secondNum, mathOperation));