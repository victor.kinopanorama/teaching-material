/**
 * Задание 5.
 *
 * Объяснить поведение каждое операции.
 */

let x, y, z;

x = 6;
// --x
// 5
// x--
// 5

// y = 15;
// z = 4;
// console.log((z = --x - y * 5));

// x = 6;
// y = 15;
// z = 4;
// console.log((y /= x + (5 % z)));

// x = 6;
// y = 15;
// z = 4;
// console.log((x += y - x++ * z));

console.log(!!'true' == !!'false'); 
// true == true (приводиться строка к булевому значению, пустая false, с текстом true)
console.log('true' == true); 
// false (приводиться к числам, строка - NaN)
console.log('true' === true); // false
console.log(NaN == 1); // false
console.log(NaN == NaN);  //  false
console.log(NaN === NaN);  // false

