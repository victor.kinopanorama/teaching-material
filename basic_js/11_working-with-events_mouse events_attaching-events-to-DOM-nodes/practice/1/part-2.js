/**
 * Задание 2.
 *
 * Улучшить скрипт из предыдущего задания.
 *
 * При наведении на кнопку  указателем мыши, выводить alert с сообщением:
 * «При клике по кнопке вы войдёте в систему.».
 *
 * Сообщение должно выводиться один раз.
 *
 * Условия:
 * - Решить задачу грамотно.
 */

const container = document.querySelector('.container');
const enterBtn = document.createElement('button');

enterBtn.textContent = 'Enter the system';

container.append(enterBtn);

enterBtn.addEventListener('mouseenter', () => alert('if you will click the button you will enter the system'), { once: true })

// //* { once: true }
// если задан этот параметр, тогда отработает только один раз
