/**
 * Задание 2.
 *
 * На экране указан список товаров с указанием названия и количества на складе.
 *
 * Найти товары, которые закончились и:
 * - Изменить 0 на «закончился»;
 * - Изменить цвет текста на красный;
 * - Изменить жирность текста на 700.
 *
 * Требования:
 * - Цвет элемента изменить посредством модификации атрибута style.
 */

const productsList = document.querySelectorAll('.store li');

productsList.forEach(elem => {

	const splitedElem = elem.textContent.split(': ');

	if (splitedElem[1] === '0' && splitedElem[1].length === 1) {

		elem.textContent = `${splitedElem[0]}: закончился`;
		elem.style.cssText = 'color: red; font-weight: 700;';	

	}

});

// productsList.forEach(e => {

// 	if(+e.innerText.replace(/[^0-9]/g, '') === 0) {

// 		let splitedElem = e.innerText.split(' ')
		
// 		e.innerText = `${splitedElem[0]} закончился`
// 		e.style.color = 'red'
// 		e.style.fontWeight = 700

// 	}

// })

