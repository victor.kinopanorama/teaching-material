/**
 * Задача 1.
 *
 * Написать функцию-помощник кладовщика replaceItems.
 *
 * Функция должна заменять указанный товар новыми товарами.
 *
 * Функция обладает двумя параметрами:
 * - Имя товара, который необходимо удалить;
 * - Список товаров, которыми необходимо заменить удалённый товар.
 *
 * Функция не обладает возвращаемым значением.
 *
 * Условия:
 * - Генерировать ошибку, если имя товара для удаления не присутствует в массиве;
 * - Генерировать ошибку, если список товаров для замены удалённого товара не является массивом.
 *
 * Заметки:
 * - Дан «склад товаров» в виде массива с товарами через.
 */

/* Дано */
let storage = [
	'apple',
	'water',
	'banana',
	'pineapple',
	'tea',
	'cheese',
	'coffee',
];

/* Решение */

const replaceItems = (deletedProduct, newProductList) => {

	if (!Array.isArray(newProductList)) throw new Error('Cписок товаров для замены удалённого товара не является массивом');

	if (!storage.includes(deletedProduct)) throw new Error('Имя товара для удаления не присутствует в массиве');

	let deletedProductIndex = storage.indexOf(deletedProduct);

	storage.splice(deletedProductIndex, 1, ...newProductList)

}

replaceItems('apple', ['carrot', 'wheat', 'grape']);

storage.forEach(e => console.log(e));