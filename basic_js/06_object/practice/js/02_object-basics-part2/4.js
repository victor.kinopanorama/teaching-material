/**
 * Задание 4.
 *
 * С помощью цикла for...in вывести в консоль все свойства
 * первого уровня объекта в формате «ключ-значение».
 *
 * Продвинутая сложность:
 * Улучшить цикл так, чтобы он умел выводить свойства объекта второго уровня вложенности.
 */

/* Дано */

const user = {
	firstName: 'Walter',
	lastName: 'White',
	job: 'Programmer',
	pets: {
		cat: 'Kitty',
		dog: 'Doggy',
	},
	zero: null
};

for (let key in user) {

	// typeof null === 'object'
	if (typeof user[key] === 'object' && user[key] !== null) {

		for (let innerKey in user[key]) {

			console.log(`${innerKey}: ${user[key][innerKey]}`);

		}

	} else {

		console.log(`${key}: ${user[key]}`);

	}

}