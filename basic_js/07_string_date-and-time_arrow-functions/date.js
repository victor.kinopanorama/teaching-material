//* ----------- Date -----------

//* new Date()
// Создаёт объект Date с текущей датой и временем
// new Date(milliseconds) - cоздаёт объект Date, значение которого равно количеству миллисекунд (1/1000 секунды), прошедших с 1 января 1970 года GMT+0
// new Date(year, month, date, hours, minutes, seconds, ms) year должен быть из 4 цифр, а отсчёт месяцев month начинается с нуля 0

//* Date.now()
// возвращает дату сразу в виде миллисекунд.Технически, он аналогичен вызову +new Date(), но в отличие от него не создаёт промежуточный объект даты, а поэтому – во много раз быстрее

const date = new Date(2011, 0, 1)

//* -- Получение компонентов даты --

//* getFullYear()
// Получить год (из 4 цифр)

console.log(date.getFullYear());

//* getMonth()
// Получить месяц, от 0 до 11

//* getDate()
// Получить число месяца, от 1 до 31

//* getDay()
// Получить номер дня в неделе. Неделя в JavaScript начинается с воскресенья, так что результат будет числом от 0(воскресенье) до 6(суббота)

const days = {
	0: 'Воскресенье',
	1: 'Понедельник',
	2: 'Вторник',
	3: 'Среда',
	4: 'Четверг',
	5: 'Пятница',
	6: 'Суббота',
};

//* getHours(), getMinutes(), getSeconds(), getMilliseconds()
// Получить соответствующие компоненты

//* getTime()
// Возвращает число миллисекунд, прошедших с 1 января 1970 года GMT+0, то есть того же вида, который используется в конструкторе new Date(milliseconds)

//* -- Установка компонентов даты --

// setFullYear(year [, month, date])
// setMonth(month [, date])
// setDate(date)
// setHours(hour [, min, sec, ms])
// setMinutes(min [, sec, ms])
// setSeconds(sec [, ms])
// setMilliseconds(ms)
// setTime(milliseconds)

let today = new Date;

today.setHours(0);

//* Date.parse(str)
// Метод разбирает строку str в таком формате и возвращает соответствующее ей количество миллисекунд. Если это невозможно, Date.parse возвращает NaN

var msUTC = Date.parse('2012-01-26T13:51:50.417Z'); // зона UTC

console.log(msUTC); // 1327571510417 (число миллисекунд)

//* .toLocaleDateString()
// Возвращает строку даты

let dateString = new Date(2012, 11, 12, 3, 0, 0);
console.log(dateString.toLocaleDateString()) // 12.12.2012