const title = document.querySelector('.hero__title');
const img = document.querySelector('.hero__img');

title.addEventListener('click', e => {
	title.classList.toggle('--text-blur');
})

img.addEventListener('click', () => {
	img.classList.toggle('--img-scale');
})
