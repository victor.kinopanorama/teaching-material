/**
 * Задание 7.
 *
 * Написать программу-калькулятор.
 *
 * Программа запрашивает у пользователя три значения:
 * - Первое число;
 * - Второе число;
 * - Математическая операция, которую необходимо совершить над введёнными числами.
 *
 * Программа должна повторно запрашивать данные, если:
 * - Любое необходимых чисел не соответствуют критерию корректного числа;
 * - Математическая операция не является одной из: +, -, /, *, **.
 *
 * Если все данные введены верно, программа вычисляет указанную операцию, и выводит в консоль результат:
 * «Над числами ЧИСЛО_1 и ЧИСЛО_2 была произведена операция ОПЕРАЦИЯ. Результат: РЕЗУЛЬТАТ.`.
 * 
 * После первого успешного выполнения программа должна спросить, есть-ли необходимость выполниться ещё раз,
 * и должна начинать свою работу сначала до тех пор, пока пользователь не ответит «Нет.».
 *
 * Когда пользователь откажется продолжать работу программы, программа выводит сообщение:
 * «✅ Работа завершена.».
 */

let firstNum;
let secondNum;
let mathOperation;
let result;

do {
	firstNum = +prompt('Введите первое число');
} while (isNaN(firstNum));

do {
	secondNum = +prompt('Введите второе число');
} while (isNaN(secondNum));

while (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '/' && mathOperation !== '*' && mathOperation !== '**') {
	mathOperation = prompt('Введите математическую операцию, которую необходимо совершить над введёнными числами');
}

switch (mathOperation) {
	case '+':
		result = firstNum + secondNum;
		break;
	case '-':
		result = firstNum - secondNum;
		break;
	case '/':
		result = firstNum / secondNum;
		break;
	case '*':
		result = firstNum * secondNum;
		break;
	case '**':
		result = firstNum ** secondNum;
		break;
}

console.log(`Над числами ${firstNum} и ${secondNum} была произведена операция ${mathOperation}. Результат: ${result}`);

let userConfirm = confirm('Желаете выполнить математическую операцию еще раз?');

while (userConfirm) {
	do {
		firstNum = +prompt('Введите первое число');
	} while (isNaN(firstNum));

	do {
		secondNum = +prompt('Введите второе число');
	} while (isNaN(secondNum));

	do {
		mathOperation = prompt('Введите математическую операцию, которую необходимо совершить над введёнными числами');
	} while (mathOperation !== '+' && mathOperation !== '-' && mathOperation !== '/' && mathOperation !== '*' && mathOperation !== '**')

	switch (mathOperation) {
		case '+':
			result = firstNum + secondNum;
			break;
		case '-':
			result = firstNum - secondNum;
			break;
		case '/':
			result = firstNum / secondNum;
			break;
		case '*':
			result = firstNum * secondNum;
			break;
		case '**':
			result = firstNum ** secondNum;
			break;
	}

	console.log(`Над числами ${firstNum} и ${secondNum} была произведена операция ${mathOperation}. Результат: ${result}`);

	userConfirm = false;

	userConfirm = confirm('Желаете выполнить математическую операцию еще раз?');
}

console.log('✅ Работа завершена')