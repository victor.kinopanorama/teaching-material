/**
 * ЗАДАНИЕ 3
 *
 * Объяснить поведение каждое операции.
 */

const first = false; // => 0
const second = true; // => 1


console.log(first == 0); // true
console.log(first === 0); // false
console.log(second === 1); // false
console.log(second == 1); // true
