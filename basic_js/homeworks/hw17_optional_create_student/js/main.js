const createStudent = () => {
   const firstName = prompt('Enter your first name');
   const lastName = prompt('Enter your last name');

   const newStudent = {
      firstName,
      lastName,
      table: {

      },
   };

   return newStudent;
};

let student = createStudent();

let grade;
let subject;
let count = 0;
let sum = 0;

do{

   subject = prompt('Enter a subject');
   
   if(subject === null) {
      break;
   }

   grade = prompt('Enter a grade');

   student.table[subject] = grade;

} while(subject !== null || grade !== null);

for(let key in student) {
   if(typeof student[key] === 'object') {
      for(let innerKey in student[key]) {
         
         if(student[key][innerKey] < 4) {
            console.log('Student not transferred to next year.');
            break;
         }

         sum += Number(student[key][innerKey]);
         count++
      }
   }
}

console.log('Congrats! Student transferred to next year.')
console.log('---------------------------------------------------');
if(sum / count >= 7) {
   console.log('Student awarded scholarship');
}
console.log('---------------------------------------------------');
console.log(`${student.firstName} ${student.lastName}`);

for(let key in student) {
   if(typeof student[key] === 'object') {
      for(let innerKey in student[key]) {
         console.log(`${innerKey} - ${student[key][innerKey]}`)
      }
   }
}