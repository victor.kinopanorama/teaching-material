const title = document.querySelector('.title');
const img = document.querySelector('.img');

title.addEventListener('click', e => {
	title.classList.toggle('text-blur');
})

img.addEventListener('click', () => {
	img.classList.toggle('img-scale');
})
