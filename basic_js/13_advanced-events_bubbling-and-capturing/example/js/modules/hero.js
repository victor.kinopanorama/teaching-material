const title = document.querySelector('.hero__main-title');
const img = document.querySelector('.hero__main-img');

title.addEventListener('click', e => {
	title.classList.toggle('text--blur');
})

img.addEventListener('click', () => {
	img.classList.toggle('img--scale');
})

