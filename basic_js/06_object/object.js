//* ---------- Objects ----------

const obj = {
	key: 'value',
	ключ: 'значение',
	'имя свойства': 'значение свойств'
}

const person = {
	name: 'Viktor',
	age: 32,
	isProgrammer: true,
	languages: ['ru', 'en', 'ua'],
	// 'complex key': 'Complex Value',
	// ['key_' + (1 + 3)]: 'Computed Key', // key_4
	greet() {
		console.log('greet from person')
	},
	info() {
		// console.log('this:', this)
		console.info('Информация про человека по имени:', this.name)
	}
}

//* Проверить обьект на наличие свойств
console.log(Object.keys(person).length === 0);

//* Обращение к свойству
// console.log(person.name)

//* Удаление свойства
// delete user.age;

//* Динамически выводить свойства объекта
// let nameKey = prompt("Что вы хотите узнать о пользователе?", "name");
// console.log(person[nameKey])

//* Динамически добавлять свойства объекта
// const key = 'isRich';
// person[key] = true;
// console.log(person.isRich)

// person.age++
// person.languages.push('de')

function makeUser(name, age) {
	return {
		name, // то же самое, что и name: name
		age   // то же самое, что и age: age
	};
}

let user = makeUser("John", 30);
console.log(user.name); // John

//* Деструктуризация
// const {name, age: personAge = 10, languages} = person

//* in -  оператор для проверки существования свойства в объекте
// let ageInObject = ("age" in person) ? alert('True!') : alert('False!')

//* for in
// Цикл для перебора всех свойств объекта
// Опасен тем, что пробегается не только по свойтвам, но и залазит в _proto_

for (let key in person) {
	// ключи
	console.log('key: ', key);  // name, age, isProgrammer
	// значения ключей
	console.log('value: ', person[key]); // Viktor, 32, true
}

//* hasOwnProperty
// Возвращает логическое значение, которое указывает на то содержит ли объект указанное cобственное (неунаследованное) свойство, или метод

// for (let key in person) {
//   if (person.hasOwnProperty(key)) {
//     console.log('key:', key)
//     console.log('value:', person[key])
//   }
// }

// Получаем массив из ключей с помощью Object.keys и перебираем свойтва
// Object.keys(person).forEach((key) => {
//   console.log('key:', key)
//   console.log('value:', person[key])
// })

//* Object.keys(obj)
// возвращает массив, содержащий все ключи объекта obj

//* Context (this)

const logger = {
	keys() {
		console.log('Object Keys: ', Object.keys(this))
	},

	keysAndValues() {
		// "key": value
		// Object.keys(this).forEach(key => {
		//   console.log(`"${key}": ${this[key]}`)
		// })
		// const self = this
		Object.keys(this).forEach(function (key) {
			console.log(`"${key}": ${this[key]}`)
		}.bind(this))
	},

	withParams(top = false, between = false, bottom = false) {
		if (top) {
			console.log('------- Start -------')
		}
		Object.keys(this).forEach((key, index, array) => {
			console.log(`"${key}": ${this[key]}`)

			// убираем делиметр после последнего элемента
			if (between && index !== array.length - 1) {
				console.log('--------------')
			}
		})

		if (bottom) {
			console.log('------- End -------')
		}
	}
}

// const bound = logger.keys.bind(person)
// bound()
// logger.keysAndValues.call(person)
logger.withParams.call(person, true, true, true)
logger.withParams.apply(person, [true, true, true])

function User(name, lastName, age) {

	this.name = name;
	this.lastName = lastName;
	this.age = age;

}

const user1 = new User('Viktor', 'Kostenko', 35);

console.log(typeof user1);
console.log(user1);

//* structuredClone(user)
// метод для глубокого копирования

// const calendarEvent = {
// 	company: "Builder.io Conf",
// 	date: new Date(123),
// 	attendees: {
// 		user1 : 'Steve',
// 		user2 : 'Suzan',
// 	}
//  }
 
//  const copied = structuredClone(calendarEvent)