const formContainer = document.querySelector('.password-form');
const confirmBtn = document.querySelector('.btn');
const inputElems = document.querySelectorAll('input');

formContainer.addEventListener('click', e => {
	let target = e.target;

	if (target.tagName !== 'I') return;


	if (target.classList.contains('active')) {

		target.previousElementSibling.type = 'text';
		target.className = 'fas fa-eye-slash icon-password';

	} else {

		target.previousElementSibling.type = 'password';
		target.className = 'fas fa-eye icon-password active';

	}
});

confirmBtn.addEventListener('click', e => {

	if (inputElems[0].value === inputElems[1].value) {

		alert('You are welcome!');

	} else {

		const errorText = document.createElement('p');
		errorText.textContent = 'Нужно ввести одинаковые значения';
		errorText.classList.add('error-text');
		confirmBtn.before(errorText);
	}

})

