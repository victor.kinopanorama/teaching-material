/**
 * 
 * Выводить в консоль, сколько дней прошло с начала года до даты, которую ввел пользователь.
 * 
 */

const minute = 1000 * 60;
const hour = minute * 60;
const day = hour * 24;
const year = day * 365;

// Колличество милисекунд с 1 января 1970
const beginOfTheYear = Date.parse("January 01, 2022");

const userDate = prompt('Enter a date', 'Month DD, YYYY');

// Колличество милисекунд пользовательской даты
const date = Date.parse(userDate);

// Колличество милисекунд с начала года
let secondsOfDate = date - beginOfTheYear;

if(userDate < date) {
   console.log('Error! Enter the correct date');
} else {
   console.log(Math.floor(secondsOfDate / day))
}