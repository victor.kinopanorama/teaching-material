/**
 * Задание 3.
 *
 * Спрашивать у пользователя 2 числа:
 * - 1 начальный год
 * - 2 конечный год (2022)
 * 
 * Посчитать количество дней между двумя введенными годами
 * Вывести сообщение "Между НАЧАЛЬНЫЙ_ГОД и КОНЕЧНЫЙ_ГОД ... дней"
 * 
 * Правило определения высокосного года:
 * - число года должно одновременно делится на 4 и не делится на 100 или делится на 400
 * 
 * Дполнительно: спрашивать числа, пока пользователь не введет правильные
 * - должно быть числом
 * - меньше 2022 и больше 1900
 * - второй год должен быть больше, чем первый
 * 
 */

let startYear;
let finishYear;
let result = 0;

do {

	startYear = prompt('Start');

	if (startYear === null) break;

	finishYear = prompt('Finish');

	if (finishYear === null) break;

} while (isNaN(startYear) || isNaN(finishYear) || startYear > 2023 || startYear < 1900 || finishYear > 2023 || startYear >= finishYear);

for (let i = startYear; i <= finishYear; i++) {

	if (i % 4 === 0 && i % 100 !== 0 || i % 400 === 0) {

		result += 366;

	} else {

		result += 365;

	}

};

console.log(`С ${startYear} по ${finishYear} прошло ${result} дней.`);