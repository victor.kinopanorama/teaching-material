const container = document.querySelector('.coords');
const text = document.querySelector('.coords__text');

function createMessage(elem) {

	let coords = elem.getBoundingClientRect();
	let left = Math.round(coords.left + coords.width + 50);
	let top = coords.top + (coords.height / 2 - 50);

	const message = document.createElement('p');
	message.textContent = 'or not to code';
	message.classList.add('message');

	console.log(coords.height);

	message.style.cssText = `position:absolute; top:${top}px; left:${left}px`;

	container.append(message)

	setTimeout(() => message.remove(), 5000)
}

createMessage(text)