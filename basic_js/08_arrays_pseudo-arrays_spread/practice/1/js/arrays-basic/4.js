let studentList = [
	{ name: 'John', gender: 'male', birthDay: '1997-7-4' },
	{ name: 'Sarah', gender: 'female', birthDay: '1992-8-17' },
	{ name: 'Sam', gender: 'male', birthDay: '1987-4-12' },
	{ name: 'Stephen', gender: 'male', birthDay: '2001-8-1' },
	{ name: 'Natalie', gender: 'female', birthDay: '1993-3-20' },
	{ name: 'Brad', gender: 'male', birthDay: '1991-2-14' },
	{ name: 'Emma', gender: 'female', birthDay: '2002-3-6' },
	{ name: 'Roberto', gender: 'male', birthDay: '1985-5-7' },
	{ name: 'Viktor', gender: 'male', birthDay: '1989-1-15' },
	{ name: 'Antonio', gender: 'male', birthDay: '1971-2-11' },
]

/**
 * 
 * Добавить к списку нового студента - Marc, 7 January 1996
 * Вывести в консоль список студентов
 * 
 */

// studentList.push({
//    name: 'Mark',
//    gender: 'male',
//    birthDay: '1996-1-7'
// });

function addStudent(list, student) {

	let splitedStudent = student.split(' ')
	let newStudent = {
		name: splitedStudent[0],
		gender: splitedStudent[1],
		birthday: splitedStudent[2],
	}

	list.push(newStudent)
}

addStudent(studentList, 'Mark male 1996-1-7');

console.log(studentList);

/**
 * Вывести в консоль имена студентов, которые начинаються на S
 */

studentList.forEach(e => {
	if (e.name.startsWith('S')) {
		console.log(e.name);
	}
})

/**
 * 
 * Создать еще 2 списка: Один со студентами муж пола, второй 
 * женского.
 * 
 */

const maleList = studentList.filter(e => e.gender === 'male');
console.log(maleList);

const femaleList = studentList.filter(e => e.gender === 'female');
console.log(femaleList);

/**
 * Вывести в консоль студентов, которым больше 20 лет
 * Вывести в консоль студентов, которым больше 30 лет
 * Вывести в консоль студентов, которым больше 40 лет
 */

const date = new Date();
const year = date.getFullYear();

function studentsByAge(list, age) {

	list.forEach(e => {

		if (year - e.birthDay.slice(0, 4) >= age) {
			console.log(e);
		}

	})

}

// studentsByAge(studentList, 20);
// studentsByAge(studentList, 30);
studentsByAge(studentList, 40);



/**
 * Вывести в консоль список студентов от младшего до старшего
 */

let sortedByAge = studentList.sort((a, b) => {

	let firstElem = a.birthDay.slice(0, 4);
	let secondElem = b.birthDay.slice(0, 4);

	if (year - firstElem > year - secondElem) return -1;
	if (year - firstElem === year - secondElem) return 0;
	if (year - firstElem < year - secondElem) return 1;

})

console.log(sortedByAge);