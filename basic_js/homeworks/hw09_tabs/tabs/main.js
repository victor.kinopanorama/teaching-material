const tabs = document.querySelector('.tabs')
const tabsElems = document.querySelectorAll('.tabs-title');
const tabsContentElems = document.querySelectorAll('ul.tabs-content > li');

let currentTarget = 'akali';

tabs.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName !== 'LI') return;

	currentTarget = target.dataset.category;

	console.log(currentTarget);

	tabsElems.forEach(elem => {

		elem.classList.remove('active');

	})

	target.classList.add('active');

	tabsContentElems.forEach(elem => {

		elem.classList.remove('visible');

		if (currentTarget === elem.dataset.category) elem.classList.add('visible')

	})

})
