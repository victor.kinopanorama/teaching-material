/**
 * Задание 1.
 *
 * Написать скрипт, который создаст элемент button с текстом «Show».
 *
 * При клике по кнопке выводить модальное окно с сообщением: «Hello World!».
 */

const container = document.querySelector('.container');
const showBtn = document.createElement('button');
showBtn.classList.add('button');
showBtn.innerText = 'Show';

document.body.prepend(showBtn);

function showModal(elem = document.body, text) {

	console.log(elem.childNodes);
	// Если нет детей, тогда создаем модалку
	if (!container.hasChildNodes()) {

		const modal = document.createElement('div');
		modal.classList.add('modal');

		elem.append(modal);

		const modalText = document.createElement('p');
		modalText.classList.add('modal__text');
		modalText.textContent = text;
		modal.prepend(modalText);

	} else {

		elem.childNodes.forEach(e => e.remove());

	}

	setTimeout(() => container.firstChild.remove(), 5000)

}

showBtn.addEventListener('click', () => showModal(container, 'Hello World!'))




