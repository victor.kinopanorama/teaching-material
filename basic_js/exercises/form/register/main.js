/**
 * Задание 2
 * 
 * - Реализуйте функционал сохранения в localStorage всех данных введенных в поля формы.
 *  (Это делается для того, чтоб сохранить введенные данные при случайной перезагрузке страницы).
 * 
 * - Каждый раз при уходе с поля (событие blur) сохранять данные в localStorage формате key = input.name, value = input value;
 * - При перезагрузке проверять наличие записи в localStorage и восстанавливать его в поле если есть;
 * - Реализовать все через одну запись в localStorage, в которой будет храниться объект с полями key = input.name, value = input value;
 * - Использовать JSON.stringify и JSON.parse
 */

const inputElems = document.querySelectorAll('input');

inputElems.forEach(elem => elem.addEventListener('blur', event => {

	const target = event.target;
	const targetName = target.name;
	const targetValue = target.value;

	let dataObj = {};

	if (localStorage.getItem('data') === null) {

		dataObj[targetName] = targetValue;

	} else {

		dataObj = JSON.parse(localStorage.getItem('data'));
		dataObj[targetName] = targetValue;

	}

	localStorage.setItem('data', JSON.stringify(dataObj))

}));

inputElems.forEach(elem => {

	let dataStorage = JSON.parse(localStorage.getItem('data'));

	if (dataStorage.hasOwnProperty(elem.name)) {
		elem.value = dataStorage[elem.name];
	}

	// for (let dataItem in dataStorage) {
	// 	if (dataItem === elem.name) {
	// 		elem.value = dataStorage[dataItem];
	// 	}
	// }

})
