//* SET
// это особый вид коллекции: «множество» значений (без ключей), где каждое значение может появляться только один раз.

const set = new Set([1, 2, 3, 3, 3, 4, 5, 5, 6,]);

// выведет только неповторяющиеся значения
console.log(set);

// в set уже есть 20, он не будет его добавлять
set.add(10).add(20).add(30).add(20).add(40);

//? проверяет наличие значения
// console.log(set.has(20));

//? размер коллекции
// console.log(set.size);

//? удаление элемента из коллекции
// console.log(set.delete(1));

//? очистить коллекцию
// console.log(set.clear());

//? перебор коллекции
// console.log(set.values());
// console.log(set.keys());

// for (let key of set) {
// 	console.log(key);
// }

//? преобразовать set в map
// console.log(set.entries());

//=======================================================

function uniqueValues(array) {

	// возвращаем массив с уникальными значениями
	return [...new Set(array)]

	// return Array.from(new Set(array))

}

console.log(uniqueValues([1, 2, 2, 2, 3, 4, 5, 5, 6, 7, 7, 7]));
