/**
 * Задание 3.
 *
 * Написать функцию, которая возвращает наибольшее число, из переданных ей
 * в качестве аргументов при вызове.
 *
 * Генерировать ошибку, если:
 * - Если функция была вызвана менее, чем с двумя аргументами;
 * - Хоть один из аргументов не является допустимым числом (в ошибке указать
 * его порядковый номер).
 *
 * Условия:
 * - Использовать тип функции arrow form;
 * - Использовать объект arguments запрещено;
 * - Обязательно использовать объект Math.
 */

const getMaxNumber = (...args) => {

	if (args.length < 2) throw new Error('Function was called with less than two arguments');

	args.forEach((num, index) => {

		if (typeof num !== 'number') {

			throw new Error(`Argument by index ${index} is not a number`)

		}

	})

	return Math.max(...args)

}

console.log(getMaxNumber(-123, 0, -5, 6, 123));
console.log(getMaxNumber());
console.log(getMaxNumber(4, 'abc'));
