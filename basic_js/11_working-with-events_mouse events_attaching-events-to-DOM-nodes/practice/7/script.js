/**
 * Зробити список покупок з можливістю
 * додавати нові товари
 *
 * В поле вводу ми можемо ввести назву нового елементу
 * для списку покупок
 * При натисканні на кнопку Add введене значення
 * потрібно додавати в кінець списку покупок, а
 * поле вводу очищувати
 *
 * ADVANCED: Додати можливість видаляти елементи зі списку покупок
 * (додати Х в кінець кожного елементу, при кліку на який відбувається
 * видалення) та відмічати як уже куплені (елемент перекреслюється при
 * кліку на нього)
 * Для реалізації цього потрібно розібратися з Event delegation
 * https://javascript.info/event-delegation
 */

const inputElem = document.querySelector('#new-good-input');
const addBtn = document.querySelector('#add-btn');
const shoppingList = document.querySelector('.shopping-list');

// функция проверки на пустую строку
function isEmpty(str) {

	if (str.trim() === '') return true;

	return false;
}

addBtn.addEventListener('click', () => {

	let inputValue = inputElem.value;

	if (!isEmpty(inputValue)) {

		const listItem = document.createElement('li');
		listItem.textContent = inputValue;
		shoppingList.append(listItem);

		const deleteBtn = document.createElement('button');
		deleteBtn.textContent = 'X';
		deleteBtn.classList.add('delete-item');
		listItem.append(deleteBtn);

		inputElem.value = '';

	}

})

//* Вариант 1

shoppingList.addEventListener('click', e => {

	let target = e.target;

	if (target.tagName !== 'BUTTON' && target.tagName !== 'LI') return;

	if (target.tagName === 'BUTTON') {
		target.parentElement.remove();
	} else {
		target.style.textDecoration = 'line-through';
	}

})

// //* Вариант 2

// shoppingList.addEventListener('click', event => {

// 	event.preventDefault();

// 	if (event.target.classList.contains('delete-item')) {
// 		event.target.parentElement.remove();
// 	} else {
// 		event.target.style.textDecoration = 'line-through';
// 	}

// });