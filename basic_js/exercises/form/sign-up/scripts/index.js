const emailInputElem = document.querySelector('#email');
const errorSpanElem = document.querySelector('.error');
const submitBtn = document.querySelector('.submit');
const inputElems = document.querySelectorAll('input');
let validateEmail = true;
let passwordMathes = true;

emailInputElem.addEventListener('focus', () => {
	emailInputElem.classList.remove('error');
	errorSpanElem.textContent = '';
	validateEmail = true;
})

emailInputElem.addEventListener('blur', (event) => {
	let target = event.target;

	if (!target.value.match(/[a-z0-9]@\w+\.\w+/)) {
		emailInputElem.classList.add('error');
		errorSpanElem.textContent = 'Invalid email';
		validateEmail = false;
	}

});

const telInputElem = document.querySelector('#tel');

telInputElem.addEventListener('input', event => {
	const target = event.target;

	if (isNaN(target.value)) {
		target.value = target.value.replace(/[^0-9]/g, '')
	}
})

const formElem = document.querySelector('form');

formElem.addEventListener('click', event => {
	const target = event.target;

	if (target.tagName === 'IMG') {

		if (target.src.endsWith('eye.svg')) {
			target.src = './svg/eye-off.svg';
			target.closest('div').previousElementSibling.type = 'text';

		} else {
			target.src = './svg/eye.svg';
			target.closest('div').previousElementSibling.type = 'password';
		}

	}
});

//* Функция сравнения паролей
function checkPassword() {
	const password = document.querySelector('#password');
	const repeatPassword = document.querySelector('#repeat_password');

	if (password.value !== repeatPassword.value) {
		alert('Passwords mismatch :/');
		password.classList.add('error');
		repeatPassword.classList.add('error');
		passwordMathes = false;
	} else {
		password.classList.remove('error');
		repeatPassword.classList.remove('error');
		passwordMathes = true;
	}

	return true;
}

//* Функция сбора инфы инпутов и создания объекта пользователя
function createUserInfoObj() {

	let userObj = {};

	inputElems.forEach(elem => {

		if (elem.name !== 'repeat_password') {
			userObj[elem.name] = elem.value;
		}

	})

	console.log(userObj)


	return userObj;

}

submitBtn.addEventListener('click', e => {

	e.preventDefault();

	checkPassword();

	if (validateEmail && passwordMathes) createUserInfoObj();

})

// //* Задание 5

// let userInfo = {}
// let numberOfUser = 1;

// const inputElems = document.querySelectorAll('input');

// function createValueObject(obj) {

// 	inputElems.forEach(elem => {

// 		if (elem.value !== '') {

// 			obj[elem.name] = elem.value;

// 		}

// 	});

// }

// submitBtn.addEventListener('click', (e) => {

// 	e.preventDefault();

// 	if (passwordMathes) {

// 		createValueObject(userInfo);

// 		if (Object.keys(userInfo).length !== 0) {

// 			localStorage[`user-${numberOfUser}`] = JSON.stringify(userInfo);
// 			numberOfUser++;

// 		}

// 	}

// });

// console.log(JSON.parse(localStorage.getItem('user-1')).login);
// console.log(JSON.parse(localStorage.getItem('user-1')).email);
// console.log(JSON.parse(localStorage.getItem('user-1')).password);


