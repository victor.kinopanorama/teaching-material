// 1. C помошью var и let мы обьявляем переменные, в которых в последующем будем менять значение, а в const задаем фиксированное значения
// 2. Переменная var игнорирует блок, в котором она была обьявлена. Например если её обьявить в if или цикле, она будет видна за пределами блока ( глобально ), мы не сможем использовать var только локально в пределах блока. Исключением служит только блок кода, который находится внутри функции, только тогда var становится локальной переменной


let userName;

do {
   userName = prompt('Enter your name');

   if(userName === null) {
      break;
   }

} while(!isNaN(userName));

let userAge;

do {
   userAge = +prompt('Enter your age');
} while(isNaN(userAge) || userAge === 0);

if(userAge < 18) {
   alert(`You are not allowed to visit this website`);
} else if(userAge > 18 && userAge <= 22) {
   let agree = confirm(`Are you sure you want to continue?`);

   if(agree) {
      alert(`Welcome, ${userName}`);
   } else {
      alert(`You are not allowed to visit this website`);
   }
} else {
   alert(`Welcome, ${userName}`);
}