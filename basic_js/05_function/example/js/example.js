const title = document.querySelector('.title');
const img = document.querySelector('.img');

title.addEventListener('click', e => {
	title.classList.toggle('text-blur');
})

img.addEventListener('click', () => {
	img.classList.toggle('img-scale');
})

const resultElem = document.querySelector('.result');

/**
 * Функция сумматор двух чисел.
 */

const getSumm = (a, b) => a + b;

resultElem.textContent = getSumm(20, 15)

/**
 * Функция принимает вид животного (кошка или собака) и возвращает его звук (лай или мяуканье).
 */

const getAnimal = animal => {

	return (animal === 'dog') ? 'Гав-Гав' : (animal === 'cat') ? 'Мяу-Мяу' : 'Нет такого животного';

}

console.log(getAnimal('cat'));


/**
 * Функция принимает длину радиуса круга и возвращает его площадь.
 */

const getSquareArea = radius => radius ** 2 * Math.PI;

console.log(getSquareArea(10));

/**
 * Функция принимает ширину и высоту прямоугольника, возвращает true если это квадрат и false если нет.	\
 */

function getParameters(width, height) {

	return (width === height) ? true : false;

}

resultElem.textContent = getParameters(20, 25);


/**
 * Функция принимает ширину и высоту прямоугольника и возвращает его периметр если это не квадрат.
 */


const getPerimeter = (width, height) => {

	return (width === height) ? 'Это не прямоугольник' : width * 2 + height * 2;

}

resultElem.textContent = getPerimeter(44, 25);


/**
 * Функция принимает аргументы ключа и значения и возвращает объект с этими ключом, значением.
 */

function getObj(key, value) {

	return {
		[key]: value,
	};

}

const user = getObj('name', 'Viktor')

console.log(user);

/**
 * Функция принимает строки в неограниченном колличестве и все их выводит одной строкой через разделитель " | ".
 */

function getString(...args) {

	return args.join(' | ');

}

console.log(getString('Hello', 'World'));


/**
 * Функция принимает Имя и возвращает HTML тег <span> с этим именем красного цвета.
 */

const getSpanWithName = name => `<span style="color: red;">${name}</span>`;

resultElem.innerHTML = getSpanWithName('Viktor');

/**
 * Функция принимает css селектор, находит первый элемент и добавляет к нему клас анимации "shadow-text".
 */

document.querySelector('h3').classList.add('shadow-text');


/**
 * Функция принимает css селектор, находит первый элемент и добавляет к нему обработчик события "click".
 * При клике выводится alert с внутренним текстом элемента.
 */

const setOnCLick = (selector) => {

	const elem = document.querySelector(selector);

	elem.addEventListener('click', () => {
		alert(elem.innerText);
	});

};

setOnCLick('h1');

