/**
 * Задание 3.
 *
 * Расширить функционал объекта из предыдущего задания:
 * - Добавить метод, который добавляет объекту новое свойство с указанным значением.
 *
 * Продвинутая сложность:
 * Метод должен быть «умным» — он генерирует ошибку при создании нового свойства, если свойство с таким именем уже существует.
 * 
 */

 const user = {
   firstName: 'Viktor',
   lastName: 'Kostenko',
   profession: 'Coder',

   sayHi() {
      console.log(`Hi, my name is ${this.firstName} ${this.lastName}!`);
   },

   changeProp() {
      let userProperty = prompt('Enter the property');
      let userValue = prompt('Enter the value');

      //* Вариант 1
      if(!this.hasOwnProperty(userProperty)) {
         throw new Error('No matching property!')
      } else {
         this[userProperty] = userValue;
      }

      //* Вариант 2
      // if(userProperty in this) {
      //    this[userProperty] = userValue;
      // } else {
      //    throw new Error('No matching property!')
      // }
   },

   addProperty() {
      let userProperty = prompt('Enter the property');
      let userValue = prompt('Enter the value');

      if(this[userProperty]) {
         console.log('Error! A property with this name already exists')
      } else {
         this[userProperty] = userValue;
      }
   }
};

// user.changeProp();

// console.log(user);

// user.sayHi();

user.addProperty();

console.log(user);