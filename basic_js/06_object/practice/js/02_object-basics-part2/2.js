/**
 * Задание 2.
 *
 * Расширить функционал объекта из предыдущего задания:
 * - Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;
 * - Добавить метод, который меняет значение указанного свойства объекта.
 *
 * Продвинутая сложность:
 * - Метод должен быть «умным» — он генерирует ошибку при совершении попытки
 * смены значения несуществующего в объекте свойства.
 * 
 */

const user = {
	firstName: 'Viktor',
	lastName: 'Kostenko',
	profession: 'Coder',
	sayHi() {
		console.log(`Hi, my name is ${this.firstName} ${this.lastName}!`);
	},
	changeProp() {
		let userProperty = prompt('Enter the property');
		let userValue;

		//* Вариант 1
		if (!this.hasOwnProperty(userProperty)) {

			throw new Error('No matching property!')

		} else {

			userValue = prompt('Enter the value');
			this[userProperty] = userValue;

		}

		//* Вариант 2
		// if(userProperty in this) {
		//    this[userProperty] = userValue;
		// } else {
		//    throw new Error('No matching property!')
		// }
	}
};

user.changeProp();

console.log(user);

user.sayHi();