/**
 * Задание 2 - 2.
 *
 * Улучшить скрипт из предыдущего задания.
 *
 * При каждом движении курсора по странице
 * менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'HELLO WORLD!';

function getRandomColor() {
	const r = Math.floor(Math.random() * 255);
	const g = Math.floor(Math.random() * 255);
	const b = Math.floor(Math.random() * 255);

	return `rgb(${r}, ${g}, ${b})`;
}

/* Решение */

const title = document.createElement('h1');
title.textContent = PHRASE;
document.body.append(title);

function colorChanger() {

	const titleText = title.innerText;
	title.innerText = '';

	for (let char of titleText) {

		title.insertAdjacentHTML('beforeend', `<span style="color:${getRandomColor()};">${char}</span>`);

	}

}

window.addEventListener('mousemove', colorChanger)