const container = document.querySelector('.tree');
const listElems = document.querySelectorAll('li');

for (let li of listElems) {

	const span = document.createElement('span');
	li.prepend(span);
	span.append(span.nextSibling);

}

container.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName !== 'SPAN') return;

	let childrensContainer = target.closest('li').querySelector('ul');

	if (!childrensContainer) return; // если нет детей

	childrensContainer.hidden = !childrensContainer.hidden;
})