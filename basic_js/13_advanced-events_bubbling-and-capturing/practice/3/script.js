/**
 * Создайте галерею изображений, в которой основное изображение изменяется при клике на уменьшенный вариант.
 *
 * Используйте делегирование событий.
 */

const gallery = document.querySelector('#gallery');
const mainImg = document.querySelector('#largeImg');

gallery.addEventListener('click', event => {

	event.preventDefault();

	const target = event.target;
	const targetParentHref = target.closest('a').href;

	mainImg.src = targetParentHref;

})

// const gallery = document.querySelector('#thumbs');
// const mainImg = document.querySelector('#largeImg');

// gallery.addEventListener('click', e => {

// 	e.preventDefault();
// 	const targetLink = e.target.getAttribute('src');

// 	mainImg.setAttribute('src', targetLink)
// })