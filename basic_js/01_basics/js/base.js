//* Переменные (variable)

//* Объявление переменной — 'let age'
//* Инициализация — это присваивание значения переменной 'let age = 32'

//* Главное отличие let от var в том, что область видимости переменной ограничивается блоком, а не функцией.
// если var обьявленна в цикле (for (var i = 0; i < prices.length; i++)) она будет доступна глобально

// camelCase
// const firstName = 'Viktor' // string
// const age = 32 // number
// const isProgrammer = true // boolean

// const _ = 'private'
// const $ = 'some value'

// const if = 'mkef' // error
// const 5withNum = 5 // error

//* 2 Конкатенация

// console.log('Имя человека: ' + firstName + ', а возраст человека: ' + age)
// console.log(`Имя человека: ${name}, а возраст человека: ${age}`)

//* шаблонная строка

// const lastName = prompt('Введите фамилию')
// alert(firstName + ' ' + lastName)

// const userInput = prompt('What is your age');

// console.log(parseInt(userInput));
// console.log(parseFloat(userInput));
