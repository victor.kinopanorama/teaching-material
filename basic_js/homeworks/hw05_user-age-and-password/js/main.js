/**
 * 
 * Экранирование используеться для вставки в выражение спец. символов и использование их, как обычные символы. Для экранирования перед символом ставится слэш.
 * 
 **/

const validateDate = date => {
   date = date.split('.')
   if (date.length !== 3
      || date[0].length !== 2
      || date[1].length !== 2
      || date[2].length !== 4) return false;

   return date.join('-');
}
 
const createNewUser = () => {
   
   const firstName = prompt('Enter your first name');
   const lastName = prompt('Enter your last name');
   const birthday = validateDate(prompt('Enter your date of birth', 'dd.mm.yyyy'));

   const newUser = {
      firstName,
      lastName,
      birthday,
      getLogin() {
         return firstName[0].toLowerCase() + lastName.toLowerCase();
      },
      getAge() {
         let dateNow = new Date();
         let userAge = dateNow.getFullYear() - this.birthday.slice(-4);

         return userAge;
      },
      getPassword() {
         return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
      }
   }

   return newUser;
}

const newUser = createNewUser();

console.log(newUser);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
