/**
 * 
 * Задание 2.
 * 
 * Получить от пользователя два числа и вывести все числа в промежутке между ними
 * 
 * Например:
 * - Пользователь ввел 1 и 7
 * Вывести в консоль 2, 3, 4, 5, 6
 * 
 * Дополнительно: выводить числа от меньшего к большему, если второе число меньше чем первое (10 и 4), тогда вывести 5, 6, 7, 8, 9
 * 
 */

let userNum1 = +prompt('Enter first number');
let userNum2 = +prompt('Enter second number');

if (isNaN(userNum1)) {
	console.log('Error, first value is not a number');
}

if (isNaN(userNum2)) {
	console.log('Error, second value is not a number');
}

if (userNum1 < userNum2) {

	for (let i = userNum1 + 1; i < userNum2; i++) {
		console.log(i)
	}

} else {

	for (let i = userNum2 + 1; i < userNum1; i++) {
		console.log(i)
	}

}