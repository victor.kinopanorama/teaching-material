const users = {
	"Ivanov": {
		age: 25,
		parent: {
			"ivanova-anna": {
				age: 45
			},
			"ivanov-alexey": {
				age: 48,
				parent: {
					"makarenko-petya": {
						age: 88,
						parent: {
							"lionenko": {
								age: 106,
								parent: {
									"petrov": {}
								}
							}
						}
					}
				}
			}
		}
	},
	"Barkaev": {
		age: 33,
		parent: {
			"sniezhko": {
				age: 55
			},
			"ignatenko": {
				age: 59,
				parent: {
					"ignatenko-boris": {
						age: 90
					}
				}
			}
		}
	}
}

function userParentsRecursion(obj) {

	if (obj.parent !== undefined) {

		for (let key in obj.parent) { // obj.parent === users[person].parent
			console.log(key);
			userParentsRecursion(obj.parent[key])
		}

	}

}

for (let person in users) {
	userParentsRecursion(users[person])
}