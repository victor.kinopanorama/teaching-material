const navContainer = document.querySelector('#navigation');

let currentTarget = 'wolverine';

navContainer.addEventListener('click', event => {

	if (event.target.tagName !== 'LI') return;

	// записываем в переменную значение атрибута элемента по которому кликнули
	currentTarget = event.target.dataset.category;

	const contentItems = document.querySelectorAll('.content-item');
	const navItems = document.querySelectorAll('li');


	contentItems.forEach(item => {

		item.classList.remove('visible');

		if (item.dataset.category === currentTarget) {
			item.classList.add('visible');
		}

	});

	navItems.forEach(item => {

		item.classList.remove('active');

		if (item.dataset.category === currentTarget) {
			item.classList.add('active');
		}

	});

});