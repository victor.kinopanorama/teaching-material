/**
 * 
 * Написать функцию, которая принимает число, которое означает на сколько дней назад мы хотим вернутся и возвращает название дня недели.
 * 
 * Например, если сегодня 19 декабря 2020, тогда указав в функцие 
 * параметр 3, мы получаем "Среда".
 * 
 * Получить от юзера колличество дней на сколько мы хотим вернутся и 
 * вывести результат с помощью функции.
 * 
 * 
 */

let userInput = +prompt('Enter the number of days');

function getDay(num) {
   let date = new Date();

   let dayOfMonth = date.getDate() - num;
   date.setDate(dayOfMonth);

   let dayOfWeek = date.getDay();

   switch(dayOfWeek) {
      case 0:
         return `Воскресенье`;
         break;
      case 1:
         return `Понедельник`;
         break;
      case 2:
         return `Вторник`;
         break;
      case 3:
         return `Среда`;
         break;
      case 4:
         return `Четверг`;
         break;
      case 5:
         return `Пятница`;
         break;
      case 6:
         return `Суббота`;
         break;
   };
}

console.log(getDay(userInput));


