/**
 *
 * При нажатии shift и "up" одновременно, увеличивать шрифт страницы на 1px,
 * а при shift и "down" уменьшать на 1px
 *
 * Максимальный размер шрифта 30px, минимальный 10px
 *
 */

let bodyFontSize = window.getComputedStyle(document.body, null).getPropertyValue("font-size");
let fontSizeNum = Number(bodyFontSize.slice(0, 2));

window.addEventListener('keyup', e => {

	if (e.shiftKey && e.code === 'ArrowUp' && fontSizeNum < 30) {

		document.body.style.fontSize = `${fontSizeNum + 1}px`;
		fontSizeNum++;

	}

	if (e.shiftKey && e.code === 'ArrowDown' && fontSizeNum > 10) {

		document.body.style.fontSize = `${fontSizeNum - 1}px`;
		fontSizeNum--;

	}

})