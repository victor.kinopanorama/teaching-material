/**
 * Задание 2.
 *
 * Написать функцию, которая определяет количество переданных аргументов, и возвращает их количество.
 */

const getArgsLength = (...args) => {

	return args.length;

}

console.log(getArgsLength('john', 'alis', 1, 'bob'));