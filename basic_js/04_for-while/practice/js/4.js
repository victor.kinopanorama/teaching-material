/**
 * Задание 4.
 *
 * Пользователь должен ввести два числа.
 * Если введённое значение не является числом,
 * программа продолжает опрашивать пользователя до тех пор,
 * пока он не введёт число.
 *
 * Когда пользователь введёт два числа, вывести в консоль сообщение:
 * «Поздравляем. Введённые вами числа: «ПЕРВОЕ_ЧИСЛО» и «ВТОРОЕ_ЧИСЛО».».
 */

//*Вариант 1

let firstNum = prompt('First number');
let secondNum = prompt('Second number');

while (isNaN(firstNum) || isNaN(secondNum)) {

	firstNum = prompt('First number');
	secondNum = prompt('Second number');

}

if (firstNum !== null && secondNum !== null) {

	console.log(`Поздравляем. Введённые вами числа: ${firstNum} и ${secondNum}.`);

}

//*Вариант 2

// let firstNum;
// let secondNum;

// do {

// 	firstNum = prompt('Enter  number');
// 	secondNum = prompt('Enter second number');

// 	if (firstNum === null || secondNum === null) break;

// } while (isNaN(firstNum) || isNaN(secondNum));


