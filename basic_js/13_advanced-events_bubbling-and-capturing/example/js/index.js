import './modules/example.js'

import './modules/navigation.js'
import './modules/hero.js'
import './modules/time.js'
import './modules/carousel.js'
import './modules/footer.js'