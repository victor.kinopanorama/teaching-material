const footerBtn = document.querySelector('#footer__btn');

let position = 20;

document.body.addEventListener('keydown', e => {

	let key = e.key;

	if (key === 'ArrowRight') {
		footerBtn.style.left = `${position + 10}px`;
		position += 10;
	} else if (key === 'ArrowLeft') {
		footerBtn.style.left = `${position - 10}px`;
		position -= 10;
	}

})

footerBtn.addEventListener('click', () => {
	window.scrollTo(0, 0)
})
