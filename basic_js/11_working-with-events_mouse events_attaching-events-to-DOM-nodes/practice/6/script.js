/**
 * Початкове значення лічильника 0
 * При натисканні на + збільшувати лічильник на 1
 * При натисканні на - зменшувати лічильник на 1
 *
 * ADVANCED: не давати можливості задавати лічильник менше 0
 *
 */


const incrementBtn = document.querySelector('#increment-btn');
const decrementBtn = document.querySelector('#decrement-btn');
const counterValue = document.querySelector('#counter');
let splitedCounterValue = counterValue.innerText.split(': ');

function increment() {

	counterValue.textContent = `${splitedCounterValue[0]}: ${Number(++splitedCounterValue[1])}`

}

function decrement() {

	if (splitedCounterValue[1] > 0) {

		counterValue.textContent = `${splitedCounterValue[0]}: ${Number(--splitedCounterValue[1])}`

	}

}

incrementBtn.addEventListener('click', increment)
decrementBtn.addEventListener('click', decrement)

// //* Дилегирование
// // document.addEventListener('click', event => {

// // 	const target = event.target;

// // 	if (target.tagName !== 'BUTTON') return;

// // 	if (target.dataset.counter === 'plus') {

// // 		increment()

// // 	} else {

// // 		decrement()

// // 	}

// // })