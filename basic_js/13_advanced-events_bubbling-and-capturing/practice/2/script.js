/**
 * Дана таблица.
 * Сделать так, чтобы по клику на ячейке она окрашивалась в цвет,
 * указанный в её атрибуте data-background-color.
 *
 * У некоторых ячеек этого атрибута нет. Их окрашивать не нужно.
 *
 * Использовать делегирование событий
 * (т.е. не вешать отдельный обработчик на каждый элемент).
 *
 * Допольнительные задания:
 * 1. По клику на следующей ячейке сбрасывать цвет фона у предыдущей кликнутой ячейки.
 */

const table = document.querySelector('.color-table');
const tableElems = document.querySelectorAll('td');

table.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName !== 'TD') return;

	const targetBgColor = target.dataset.backgroundColor;

	paintElem(target, targetBgColor);

})

function paintElem(elem, bgColor) {

	tableElems.forEach(td => td.style = '');

	elem.style.backgroundColor = bgColor;
}