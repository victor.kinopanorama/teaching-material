/**
 * 
 * Цикл forEach используется, как метод массива. Он вызывает callback  
 * функцию для каждого элемента массива (выполняет какое-то действие с 
 * каждым элементом массива).
 * 
 */

function filterBy(arr, elemType) {

	return arr.filter(e => typeof e !== elemType);

}

console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));

