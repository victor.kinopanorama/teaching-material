/**
 * Считать количество нажатий на space, enter и Backspace клавишы
 * 
 * Отобразить результат на странице
 *
 */

const enter = document.querySelector('#enter-counter');
const space = document.querySelector('#space-counter');
const backspace = document.querySelector('#backspace-counter');

window.addEventListener('keyup', event => {

	let key = event.code;

	(key === 'Enter') ? enter.textContent++ :
		(key === 'Backspace') ? backspace.textContent++ :
			(key === 'Space') ? space.textContent++ :
				console.log('Wrong key');

})

// document.addEventListener('keydown', event => {
// 	switch (event.code) {
// 		case 'Enter':
// 			const enterCounter = document.getElementById('enter-counter');
// 			enterCounter.innerText++;
// 			break;

// 		case 'Space':
// 			const spaceCounter = document.getElementById('space-counter');
// 			spaceCounter.innerText++;
// 			break;

// 		case 'Backspace':
// 			const backspaceCounter = document.getElementById('backspace-counter');
// 			backspaceCounter.innerText++;
// 			break;
// 	}
// });