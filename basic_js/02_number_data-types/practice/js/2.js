/**
 * ЗАДАНИЕ 2
 *
 * Объяснить поведение каждой операции.
 */

const number = 123;
const string = 'hello';

console.log(number + string); // "123hello"
console.log(number - string); //NaN
console.log(number * string); //NaN
