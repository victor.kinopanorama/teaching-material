/**
 * 
 * Выводить день недели по дате рождения.
 * 
 * - Пользователь вводит дату рождения в формате 'YYYY-MM-DD', например '1992-04-03' и должно выводится названия дня недели, который тогда был.
 * 
 * 
 */

let userBirth = prompt('Введите дату своего рождения');
let dateOfBirth = new Date(userBirth);
let dayOfBirth = dateOfBirth.getDay();

switch(dayOfBirth) {
   case 0:
      console.log(`Вы родились в воскресенье`);
      break;
   case 1:
      console.log(`Вы родились в понедельник`);
      break;
   case 2:
      console.log(`Вы родились во вторник`);
      break;
   case 3:
      console.log(`Вы родились в среду`);
      break;
   case 4:
      console.log(`Вы родились в четверг`);
      break;
   case 5:
      console.log(`Вы родились в пятницу`);
      break;
   case 6:
      console.log(`Вы родились в субботу`);
      break;
};


