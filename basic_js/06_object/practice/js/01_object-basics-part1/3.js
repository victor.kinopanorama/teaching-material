/**
 * Создать объект машины со свойствами:
 * 
 * марка - BMW
 * модель - M3
 * мощность двигателя - 2500
 * тип двигателя - бензин
 * колличество цилиндров - 4
 * цена - 34000
 * наличие в салоне - есть
 * 
 * - вывести в консоль цену
 * - изменить цену на 32800
 * - вывести в консоль цену
 * - отметить, что нет в наличии
 * - вывести в консоль объект
 * - удалить цену
 * - вывести в консоль объект
 * 
 * 
 */

const car = {
	brand: 'BMW',
	model: 'X3',
	power: 2500,
	engineType: 'gasoline',
	cylinder: 4,
	price: 34000,
	isAvailable: true
}

console.log(car.price);

car.price = 32800;
console.log(car.price);

car.isAvailable = false;
console.log(car);

delete car.price;
console.log(car)
