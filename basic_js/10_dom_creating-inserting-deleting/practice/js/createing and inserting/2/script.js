/**
 * Задание 2.
 *
 * Написать функцию-фабрику квадратов createSquares.
 *
 * Функция обладает одним параметром — количеством квадратов для создания.
 *
 * Если пользователь ввёл количество квадратов для создания в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Максимальное количество квадратов для создания — 10.
 * Если пользователь решил создать более 10-ти квадратов — сообщить ему о невозможности такой операции
 * и запрашивать данные о количестве квадратов для создания до тех пор, пока они не будут введены корректно.
 *
 * Размер каждого квадрата в пикселях нужно получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в недопустимом формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *
 * Цвет каждого квадрата необходимо запросить после введения размера квадрата в корректном виде.
 *
 * Итого последовательность ввода данных о квадратах выглядит следующим образом:
 * - Размер квадрата n;
 * - Цвет квадрата n;
 * - Размер квадрата n + 1;
 * - Цвет квадрата n + 1.
 * - Размер квадрата n + 2;
 * - Цвет квадрата n + 3;
 * - и так далее...
 *
 * Если не любом этапе сбора данных о квадратах пользователь кликнул по кнопке «Отмена»,
 * необходимо остановить процесс создания квадратов и вывести в консоль сообщение:
 * «Операция прервана пользователем.».
 *
 * Все стили для каждого квадрата задать через JavaScript за раз.
 *
 * Тип элемента, описывающего каждый квадрат — div.
 * Задать ново-созданным элементам CSS-классы: .square-1, .square-2, .square-3 и так далее.
 *
 * Все квадраты необходимо сделать потомками body документа.
 */

let numberOfSquares;

do {

	numberOfSquares = prompt('Enter squares quantity', 'max 10');

	if (numberOfSquares === null) break;

	if (numberOfSquares > 10 || numberOfSquares < 0) {

		while (numberOfSquares > 10 || numberOfSquares < 1 || isNaN(numberOfSquares) || numberOfSquares === '') {

			if (numberOfSquares === null) {

				console.log('The operation was aborted by the user.');

				break;
			}

			numberOfSquares = prompt('Operation not possible. Please enter a quantity less than ten');

		}

	}

} while (isNaN(numberOfSquares) || numberOfSquares === '' || numberOfSquares === 0);


function createSquares(squaresSum) {

	let squareSize;
	let squareColor;
	let maxSum = 1;

	do {

		squareSize = prompt('Enter the size of the square')

		while (isNaN(squareSize) || squareSize === '' || squareSize < 1) {

			if (squareSize === null) {
				console.log('The operation was aborted by the user.');
				break;
			}

			squareSize = prompt('Enter the size of the square')

		}

		squareColor = prompt('Enter the color of the square')

		while (!isNaN(squareColor) || squareColor === '') {

			if (squareColor === null) {
				console.log('The operation was aborted by the user.');
				break;
			}

			squareColor = prompt('Enter the color of the square')

		}

		const divElem = document.createElement('div');

		divElem.classList.add(`square-${maxSum}`);
		divElem.style.cssText = `width:${squareSize}px; height:${squareSize}px; background-color:${squareColor}`;

		document.body.append(divElem);

		maxSum++;

	} while (maxSum <= squaresSum)

}

if (numberOfSquares) createSquares(numberOfSquares);