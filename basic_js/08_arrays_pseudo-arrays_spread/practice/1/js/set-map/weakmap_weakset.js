//* WEAKMAP
// предотвращает учечку данных
// ключами могут являться только объекты

let obj = { name: 'weakmap' }

// const arr = [obj];

// obj = null;

// console.log(arr[0]);

const map = new WeakMap([
	[obj, 'object data']
])

// методы get set has delete

// ==========================================

const cache = new WeakMap()

function cacheUser(user) {

	if (!cache.has(user)) {
		cache.set(user, Date.now())
	}

	return cache.get(user)
}

let helen = { name: 'Helen' };
let adam = { name: 'Adam' };

cacheUser(helen);
cacheUser(adam);

// после удаления объекта в кеше не хранится ничего лишнего
helen = null;

console.log(cache.has(helen));
console.log(cache.has(adam));

// ==========================================

//* WEAKSET
// ключами могут являться только объекты

const users = [
	{ name: 'Elena' },
	{ name: 'Alex' },
	{ name: 'Viktor' }
]

const visits = new WeakSet();

visits.add(users[0]).add(users[1]);

console.log(visits.has(users[0]));
console.log(visits.has(users[1]));