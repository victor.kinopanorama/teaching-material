/**
 * 
 * Вывести модальное ок с вариантами ответа Ok/Cancel
 * и посчитать, сколько времени в секундах прошло с момента открытия 
 * окна до закрытия.
 * 
 */

let start = Date.now();

const modal = confirm('Make Your Choice');

let finish = Date.now();

console.log(`С момента открытия до закрытия окна прошло ${((finish - start) / 1000).toFixed()} секунды.`);

// console.log(`С момента открытия до закрытия окна прошло ${Math.floor((finish - start) / 1000)} секунды.`);


