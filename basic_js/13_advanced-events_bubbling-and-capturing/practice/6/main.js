/**
 * 
 * Есть список товаров, а в дата атрибуте price указана цена товара.
 * При нажатии на товар выводить в блок price цену товара.
 * 
 * ADVANCED: Добавиь в html текстовое поле, куда можно вводить цену и кнопку Filter.
 * Отображать только те товары, цена которых указана пользователем.
 * Например, при введении 1000 и нажатии Filter в списке появляется только те товары цена которых меньше 1000.
 * 
 */

const goodsList = document.querySelector('.goods-list');
const goodsListItems = document.querySelectorAll('.goods-list li');
const productPrice = document.querySelector('.product-price');
const inputElem = document.createElement('input');
const filterBtn = document.createElement('button');

filterBtn.textContent = 'Filter';

productPrice.after(inputElem);
inputElem.after(filterBtn)

goodsList.addEventListener('click', event => {

	productPrice.textContent = `Price: ${event.target.dataset.price}`;

})

filterBtn.addEventListener('click', event => {

	const value = inputElem.value;

	goodsListItems.forEach(item => {

		if (+item.dataset.price > value) {

			item.remove()

		}

	})

})