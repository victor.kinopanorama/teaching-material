/**
 * Создать объект студента с именем, фамилией и оценками по разделам
 * html - 70
 * css - 89
 * js - 77
 * python - 65
 * 
 * Спрашивать у пользователя, по какому разделу он хочет узнать оценку
 * и выводить оценку в консолью.
 * 
 * Если такого раздела нет сообщить об этом.
 * 
 */

const student = {
	firstName: 'Mark',
	lastName: 'Walberg',
	age: 37,
	grades: {
		html: 70,
		css: 89,
		js: 77,
		python: 65,
	},
}

let userInput = prompt('Введие раздел для вывода оценки');
let userInputLower = userInput.toLowerCase();

//* Вариант 1
if (student.grades.hasOwnProperty(userInputLower)) {

	console.log(`Ваша оценка по ${userInputLower}: ${student.grades[userInputLower]}`);

} else {
	console.log('Нет подходящего раздела')
}

//* Вариант 2
// if(typeof student.grades[userInputLower] === 'undefined') {
//    console.log('Нет подходящего раздела')
// } else {
//    console.log(`Ваша оценка по ${userInputLower}: ${student.grades[userInputLower]}`);
// }