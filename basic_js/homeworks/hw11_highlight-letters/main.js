/**
 * События клавиатуры не рекомендуется для работы с input, т.к. у полей input есть свои собственные события отслеживающие изминение состояния:
 * change, input, cut, copy, paste.
 * События клавиатуры в свою очередь необходимо использовать, если требуется взаимодействие с клавиатурой, напр.: улавливать комбинации * клавиш, альты, стрелки
 *
 */

const btnElems = document.querySelectorAll('.btn-wrapper > .btn');

window.addEventListener('keyup', event => {

	const key = event.key;

	console.log(key);

	btnElems.forEach(btn => btn.style = '');

	btnElems.forEach(btn => {

		let btnText;

		(key !== 'Enter') ? btnText = key.toUpperCase() : btnText = 'Enter';

		if (btnText === btn.textContent) btn.style.backgroundColor = 'red';

	})
})


// const buttons = document.querySelectorAll('button');

// window.addEventListener('keyup', e => {

// 	let btnName = e.key;

// 	buttons.forEach(e => {

// 		e.style = '';

// 		if (btnName.toLowerCase() === e.innerText.toLowerCase()) {

// 			e.style.backgroundColor = 'blue';

// 		}

// 	})

// })