/**
 * Задание 4.
 *
 * Написать улучшенную функцию-счётчик countAdvanced.
 *
 * Функцию-счётчик из предыдущего задания расширить дополнительным функционалом:
 * - Добавить ей третий параметр, и выводить в консоль только числа, кратные значению из этого
 *  параметра;
 * - Генерировать ошибку (throw new Error), если функция была вызвана не с тремя аргументами;
 * - Генерировать ошибку, если любой из аргументов не является допустимым числом.
 */

function countAdvanced(startNum, endNum, multiplyNum) {
	if (arguments.length !== 3) {
		throw new Error("Передайте 3 аргумента");
	}

	if (
		isNaN(startNum)
		|| isNaN(endNum)
		|| isNaN(multiplyNum)
		// typeof startNum !== 'number' проверяет не передали ли нам NaN (typeof NaN === number)
		|| typeof startNum !== 'number'
		|| typeof endNum !== 'number'
		|| typeof multiplyNum !== 'number'
	) {
		throw new Error("Один из аргументов не является допустимым числом");
	}

	if (startNum > endNum) {
		console.log("⛔️ Ошибка! Счёт невозможен");
	} else if (startNum === endNum) {
		console.log("⛔️ Ошибка! Нечего считать.");
	} else {
		console.log("🏁 Отсчёт начат.");

		for (let i = startNum; i <= endNum; i++) {
			if (i % multiplyNum === 0) {
				console.log(i);
			}
		}

		console.log("✅ Отсчёт завершен.");
	}
}

console.log(countAdvanced(5, 20, 4, 5));