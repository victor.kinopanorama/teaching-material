/**
 * Задание 1.
 *
 * Получить элемент с классом .remove.
 * Удалить его из разметки.
 * .remove()
 *
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 * .classList.add(class)
 * .classList.remove(class)
 *
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах:
 * в одну строку и в две строки.
 * .classList.replace(oldClass, newClass)
 */

// *  Удаление ПЕРВОГО элемента с классом .remove */
// const elementToRemove = document.querySelector('.remove');
// elementToRemove.remove();

// * Изменение: способ 1 */
// const elementBigger = document.querySelector('.bigger');
// elementBigger.classList.remove('bigger');
// elementBigger.classList.add('active');

// * Изменение: способ 2 */
// elementBigger.classList.replace('bigger', 'active');

// * Удаление ВСЕХ элементов remove с классом .remove */
// Выбираем так:
// const removeEls = Array.from(document.getElementsByClassName('remove'));
// Или так:
// const removeEls = document.querySelectorAll('.remove');

// Перебираем все и удаляем:
// for (let el of removeEls) {
//     el.remove();
// }

// removeEls.forEach(e => e.remove())