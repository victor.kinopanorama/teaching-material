const linksContainer = document.querySelector('.links');

let currentTarget;

linksContainer.addEventListener('click', event => {

	if (event.target.tagName !== 'LI') return;

	currentTarget = event.target.dataset.name;

	const textItems = document.querySelectorAll('.text');
	const linkItems = document.querySelectorAll('.links__item');

	textItems.forEach(item => {

		item.classList.remove('active-text');

		if (item.dataset.name === currentTarget) {
			item.classList.add('active-text')
		}

	})

	linkItems.forEach(item => {

		item.classList.remove('active-link');

		if (item.dataset.name === currentTarget) {
			item.classList.add('active-link')
		}

	})

})
