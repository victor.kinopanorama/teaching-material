/**
 * Создать объект телефона со войствами:
 * название - iPhone
 * модель - 13
 * цена - 28700
 * колличество на складе - 8
 * 
 * Добавить фунцию для вычисления общей стоимость товаров на складе
 * 
 * - вывести в консоль общую стоимость товаров
 * - дать пользователю возможность изменить колличество на складе
 * через модальное окно
 * - вывести в консоль общую стоимость товаров
 * 
 */

const phone = {
	name: 'iPhone',
	model: '13',
	price: 28700,
	inStock: 8,
	getTotalSum() {
		return this.price * this.inStock;
	},
}

const userInput = prompt('Enter quantity');

console.log(phone.getTotalSum());

phone.inStock = userInput;

console.log(phone.getTotalSum());