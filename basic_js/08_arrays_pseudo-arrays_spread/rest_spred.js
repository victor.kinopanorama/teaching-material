//* ---------- rest & spred ----------

//* rest (остаточные параметры)

// Остаточные параметры могут быть обозначены через три точки ...
// Остаточные параметры соберут все остальные аргументы в массив

function showName(firstName, lastName, ...titles) {
   alert(`${firstName} ${lastName}`); // Viktor Kostenko

   // Оставшиеся параметры пойдут в массив
   // titles = ["Rich", "Coder"]
   console.log(titles[0]); // Rich
   console.log(titles[1]); // Coder
   console.log(titles.length); // 2
}

showName("Viktor", "Kostenko", "Rich", "Coder");

//* arguments
// Все аргументы функции находятся в псевдомассиве arguments

// function showArg() {
//
//    Объект arguments можно перебирать
//    for (let arg of arguments) {
//       console.log(arg);
//    }
// }

// showArg(1, 2, 3, 4, 5)

//* spred (oператор расширения)

// Оператор раскрывает массив в список аргументов

let arrNum = [3, 5, 1];
let arrStr = [a, b, c]

console.log(Math.max(...arr));

// Оператор расширения можно использовать и для слияния массивов

let newArray = [...arrNum, 54, true, 'string', ...arrStr]

console.log(newArray);

// Превращает строку в массив символов

let str = "Hello";

console.log([...str]);
