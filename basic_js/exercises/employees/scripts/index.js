const employees = [
	{
		name: 'Brandy Hambleton',
		age: 31,
		position: 'designer',
		avatar: 'https://i.pravatar.cc/250?v=1',
		isOnVacation: false,
		id: 132,
	},
	{
		name: 'Killa Shikoba',
		age: 25,
		position: 'QA',
		avatar: 'https://i.pravatar.cc/250?v=2',
		isOnVacation: false,
		tel: '+12244656767',
		id: 12,
	},
	{
		name: 'Colten Walsh',
		age: 46,
		position: 'lead developer',
		avatar: 'https://i.pravatar.cc/250?v=3',
		isOnVacation: false,
		tel: '+443236000887',
		id: 32,
	},
	{
		name: 'Stace Rounds',
		age: 16,
		position: 'intern',
		avatar: 'https://i.pravatar.cc/250?v=4',
		isOnVacation: false,
		id: 11,
	},
	{
		name: 'Joel James',
		age: 21,
		position: 'key developer',
		avatar: 'https://i.pravatar.cc/250?v=5',
		isOnVacation: true,
		tel: '+1888983923',
		id: 54,
	},
	{
		name: 'Regan Duke',
		age: 22,
		position: 'key developer',
		avatar: 'https://i.pravatar.cc/250?v=6',
		isOnVacation: false,
		tel: '+1878734345888',
		id: 56,
	},
	{
		name: 'Arnav Crouch',
		age: 43,
		position: 'backend developer',
		avatar: 'https://i.pravatar.cc/250?v=7',
		isOnVacation: false,
		tel: '+344654546467',
		id: 516,
	},
	{
		name: 'Regan Bender',
		age: 24,
		position: 'junior developer',
		avatar: 'https://i.pravatar.cc/250?v=8',
		isOnVacation: true,
		id: 116,
	},
	{
		name: 'Esmay Johnston',
		age: 22,
		position: 'QA',
		avatar: 'https://i.pravatar.cc/250?v=9',
		isOnVacation: false,
		tel: '+12676678123',
		id: 74,
	},
];

const filteredEmployess = employees.filter(e => !e.isOnVacation);
const sortedEmployeesById = filteredEmployess.sort((a, b) => a.id - b.id);

const listContainer = document.querySelector('#container');

sortedEmployeesById.forEach((elem, ind) => {

	const resultHTML = `
		<li>
 			<p class="number">${ind + 1}</p>
 			<img class="avatar" src=${elem.avatar} alt="${elem.name} avatar">
			<p class="name">${elem.name}</p>
 			<p class="position">${elem.position}</p>
 			<p class="age">Age: ${elem.age}</p>
			<p class="id">id: ${elem.id}</p>
  		</li>`;

	listContainer.insertAdjacentHTML('beforeend', resultHTML);

});

const cardsElems = document.querySelectorAll('li');

// Добавляем иконку delete к каждому элементу
cardsElems.forEach(elem => elem.insertAdjacentHTML('beforeend', '<button class="delete-button"><img src="assets/delete-icon.png" /></button>'));

// Навешиваем на все LI обработчик событий, а в обработчик передаем функцию showValue c индексом выбранной LI
cardsElems.forEach((elem, index) => elem.addEventListener('click', event => {

	const target = event.target;

	if (target.tagName === 'BUTTON') {

		target.closest('li').remove();

	} else {

		showValue(index);

	}

}))

function showValue(index) {

	const userInput = prompt('Enter search option');

	if (userInput === 'name') {

		alert(sortedEmployeesById[index].name);

	} else if (userInput === 'tel') {

		if (sortedEmployeesById[index].tel) {

			alert(sortedEmployeesById[index].tel);

		} else {

			alert('No number found');

		}

	} else {

		alert('Invalid search option');

	};

};

document.body.insertAdjacentHTML('afterbegin', '<input class="search" type="text" name="search">');

const searchInputElem = document.querySelector('.search');

searchInputElem.addEventListener('blur', e => {

	const userInput = e.target.value;

	showSearchCard(listContainer, userInput);

});

function showSearchCard(container, userInput) {

	sortedEmployeesById.forEach((elem, ind) => {

		if (userInput.toLowerCase() === elem.name.toLowerCase()) {

			cardsElems.forEach(elem => elem.remove());

			const resultHTML = `
			<li>
 				<p class="number">${ind + 1}</p>
 				<img class="avatar" src=${elem.avatar} alt="${elem.name} avatar">
				<p class="name">${elem.name}</p>
 				<p class="position">${elem.position}</p>
 				<p class="age">Age: ${elem.age}</p>
				<p class="id">id: ${elem.id}</p>
  			</li>`;

			container.insertAdjacentHTML('beforeend', resultHTML);

		}

	})

};