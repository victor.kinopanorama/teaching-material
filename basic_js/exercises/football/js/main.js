const field = document.querySelector('.field');
const ball = document.querySelector('#field__ball');

field.addEventListener('click', event => {

	ball.style.cssText = `top:${event.clientY - (ball.clientHeight / 2)}px; left:${event.clientX - (ball.clientWidth / 2)}px;`;

	if (event.clientX < 100) showGolaModal();
	if (event.clientX > 1820) showGolaModal();

})

function showGolaModal() {

	const modal = document.createElement('div');
	const modalText = document.createElement('p');

	modal.classList.add('field__modal');
	modalText.classList.add('field__modal-text');

	modalText.textContent = 'goal!';

	field.append(modal);
	modal.append(modalText)

	setTimeout(() => modal.remove(), 2000);

}