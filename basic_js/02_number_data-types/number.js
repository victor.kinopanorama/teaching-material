//* ---------- Number ----------

// const num = 42 // Целое число (integer)
// const float = 42.42 // Плавающее (float)
// const pow = 10e3 // e3 три ноля

// Максимальное число для безопасных операций
// console.log(Number.MAX_SAFE_INTEGER)

// Если число поделить на ноль, выйдет бесконечность
// console.log(2 / 0)

// console.log(typeof NaN) // Number
// isNan() проверяет являеться ли значение числом

// console.log(isFinite(42)) проверяет являеться ли значение бесконечностью
// Иногда isFinite используется для проверки, содержится ли в строке число
// console.log(isFinite(num))

// num.toString(16) - метод возвращает строковое представление числа num в системе счисления base

//* parseInt и parseFloat

// parseInt возвращает целое число
// console.log( parseInt('100px') ); // 100

// parseFloat возвращает число с плавающей точкой
// console.log( parseFloat('12.5em') ); // 12.5

//* BigInt
// BigInt - это специальный числовой тип, который предоставляет возможность работать с целыми числами произвольной длины

// console.log(90071992547409919999999n)
// console.log(90071992547409919999999.23231n) // error

//* toFixed и Math.round
// console.log(0.4 + 0.6) // 0.600000000001

// toFixed(n) - округляет число до n знаков после запятой и возвращает строковое представление результата
// console.log((0.4 + 0.6).toFixed(2)) // 0.60

// Math.round(n) - n будет округлён до ближайшего большего целого
// num = Math.round(20.4) // 20
// num = Math.round(20.5) // 21

//* Math

// Math.PI - Отношение длины окружности круга к его диаметру, приблизительно равно 3,14159
// console.log(Math.PI)

// console.log(Math.sqrt(25)) // Вычисляет квадратный корень
// console.log(Math.pow(5, 3)) // Возводит число в степень
// console.log(Math.abs(-42)) // 42 Возвращает абсолютное значение числа

// Math.max - максимальное число из списка аргументов
// console.log(Math.max(42, 12, 23, 11, 422)) // 422
// Math.min - минимальное число из списка аргументов
// console.log(Math.min(42, 12, 23, 11, 422)) // 11

// Math.floor - округление в меньшую сторону
// console.log(Math.floor(4.9)) // 4

// Math.ceil - округление в большую сторону
// console.log(Math.ceil(4.9))


// console.log(Math.trunc(4.9)) // Возвращает целую часть числа в меньшую сторону, убирая дробные цифры

// Math.random - Псевдослучайное число с плавающей запятой
// console.log(Math.floor(Math.random() * 100)) // Целое число от 0 до 100

// Функция возвращает число в диапазоне между заданными аргументами
// function getRandomBetween(min, max) {
//    return Math.floor(Math.random() * (max - min + 1) + min)
// }

// console.log(getRandomBetween(10, 42))
