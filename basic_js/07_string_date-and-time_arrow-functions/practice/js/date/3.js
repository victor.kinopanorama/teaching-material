/**
 * Написать функцию, которая:
 * - принимает дату в виде строки в формате 'yyyy-mm-dd'
 * - возвращает дату предыдущего дня
 * 
 * дату нужно вернуть в текущей локали браузера при помощи функции .toLocaleDateString()
 */

function getDate(date) {
	let dateNow = new Date(date)

	dateNow.setDate(dateNow.getDate() - 1);

	return dateNow.toLocaleDateString()
}

console.log(getDate('2024-11-02'));


